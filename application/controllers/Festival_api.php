<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Festival_api extends REST_Controller  {

	function __construct() {
		parent::__construct();
	}


	function college_post(){
		
		$email = $this->post('email');
		$collegeData = array(
			'CollegeName' 	=> 	$this->post('name'),
			'Address'		=>	$this->post('address'),
			'EmailId'		=>	$email,
			'PhoneNo'		=>	$this->post('phno'),
			'MobileNo'		=>	$this->post('mobile'),
			'Password'=> $this->post('password'),
			 );
		$isEmailExist = $this->CollegeModel->get_college_by_mail($email);
		if(count($isEmailExist)>0){
			$this->response([
				'resultCode'=>0,
				'message'=>'Email Id already exist'
			]);
		}
		$result = $this->CollegeModel->insert_entry($collegeData);

		

		if($result>0){
			$loginData = array(
				'EmailId' => $this->post('email'),
				'Password'=> $this->post('password'),
				'RoleType'=> 2,
				'CollegeId'=>$result, 
			);
			$this->AccountModel->insert_login($loginData);
			$this->response([
				'resultCode'=>1,
				'message'=>'sucess',
				'CollegeId'=>$result,
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'message'=>'Some error occoured .Please try again later'
			]);
		}
	}


	
	function college_get(){
		$email = $this->get("email");
		$resultData = $this->CollegeModel->get_college_by_mail($email);
		if(count($resultData)>0){
			$this->response([
				'resultCode'=>1,
				'data'=>$resultData,
			]);
		}
		
	}

	function item_deatils_by_student_get(){
		$studentId = $this->get("studentid");
		$itemType  = $this->get("itemtype");
		$collegeId  = $this->get("collegeid");
		$resultData= $this->ItemsModel->get_all_items_by_type_student($itemType,$studentId,$collegeId);
		if(count($resultData)>0){
			$this->response([
				'resultCode'=>1,
				'data'=>$resultData,
				'msg' =>'Result Found'
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'data'=>[],
				'msg' =>'No Result Found'
			]);
		}


	}

	function delete_item_deatils_by_item_get(){
		$studentId = $this->get("studentid");
		$itemId  = $this->get("itemId");
		$resultData= $this->ItemsModel->delete_items_by_type_item($itemid,$studentId);
		if(count($resultData)>0){
			$this->response([
				'resultCode'=>1,
				'data'=>$resultData,
				'msg' =>'Result Found'
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'data'=>[],
				'msg' =>'No Result Found'
			]);
		}
	}

	function item_post(){
		$itemData = array(
			'ItemName'         => $this->post('itemname'),
			'ItemDescription'  => $this->post('itemdesc'),
			'ItemTypeId'	   => $this->post('itemtype'),
			'Members'          =>$this->post('member'),
		);

		$result = $this->ItemsModel->insert_entry($itemData);
		if($result>0){
			$this->response([
				'resultCode'=>1,
				'msg' =>'Insert Sucess'
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'data'=>[],
				'msg' =>'Faill'
			]);

		}
		
	}
	function college_by_id_get($id){
		$result = $this->CollegeModel->get_college_by_id($id);
		if($result){
			$this->response([
				'resultCode'=>1,
				'data'=>$result
				
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'data'=>[],
				'faill'
				
			]);
		}
	}

	function college_edit_post(){
		$email = $this->post('email');
		$collegeid = $this->post('collegeid');
		$collegeData = array(
			'CollegeName' 	=> 	$this->post('name'),
			'Address'		=>	$this->post('address'),
			'EmailId'		=>	$email,
			'PhoneNo'		=>	$this->post('phno'),
			'MobileNo'		=>	$this->post('mobile'),
			'Password'		=> $this->post('password'),
			 );
		$loginData = array(
				'EmailId' => $this->post('email'),
				'Password'=> $this->post('password'),
				 
		);
		$result = $this->CollegeModel->update_entry($collegeData,$collegeid,$loginData);
		if($result>0){
			$this->response([
				'resultCode'=>1,
				'message'=>'sucess'
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'message'=>'Some error occoured .Please try again later'
			]);
		}
	}
	public function get_student_get(){
		$admno = $this->get('admissionno');
		$collegeid = $this->get('collegeid');
		
		$result = $this->StudentModel->get_student_by_admission_college($collegeid,$admno);
		
		if(count($result)>0){
			$this->response([
				'resultCode'=>1,
				'message'=>'sucess',
				'data'=>$result,
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'message'=>'No Result Found'
			]);
		}
	}
	public function item_get($id){
		$result = $this->ItemsModel->item_by_id($id);
		if(count($result)>0){
			$this->response([
				'resultCode'=>1,
				'message'=>'sucess',
				'data'=>$result,
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'message'=>'No Result'
			]);
		}
	}


	public function insert_group_participant_post(){

		$ItemType  = 1;
		$CollegeId = $this->post("collegeid");
		$ItemId    = $this->post("itemid");
		$AdmissionNo= $this->post("admno");
		$name = $this->post("name");
		$mobile = $this->post("mobile");
		$IsStudentExit = $this->post("isstudentexit");
		$StudentId=0;
		$studentData = array(
			'AdmissionNo' 	=> $AdmissionNo,
			'Name' 			=> $name,
			'Class' 		=> $this->post("class"),
			'Course' 		=> $this->post("course"),
			'CollegeId' 	=> $CollegeId,
			'PhoneNo' 		=> $mobile,
			'EmailId' 		=> $this->post("email"),
		);

		if($name=="" || $mobile=="" || $AdmissionNo==""){
			$this->response([
					'resultCode'=>0,
					'message'=>'Must fill required fileds',

				]);
		}

		$resultItemRegisted = $this->ItemsModel->get_all_registered_participant_group($ItemId,$CollegeId);
		$itemDetails = $this->ItemsModel->item_by_id($ItemId);
		//Rules 1-Check if alreday registed or not
		if(count($this->ItemsModel->item_register_rule_single_3($CollegeId,$AdmissionNo,$ItemId))>0){
			$this->response([
					'resultCode'=>0,
					'message'=>'Student Already register in the same item',

				]);

		}elseif (count($this->ItemsModel->item_register_rule_group_1($CollegeId))>6) {
			$this->response([
					'resultCode'=>0,
					'message'=>' Each college is allowed to participate only in a maximum of
6',

				]);
		}elseif($itemDetails[0]->Members==count($resultItemRegisted)){
			$this->response([
					'resultCode'=>0,
					'message'=>'Already registerd all participate in this item',

				]);
		}


		$this->ItemsModel->insert_group_inividual_entry_group($ItemId,$CollegeId);
		//Chcek if student Registed or not
		if(count($this->StudentModel->get_student_by_admission_college($CollegeId,$AdmissionNo))>0){
			$StudentId= $this->StudentModel->update_student($studentData,$AdmissionNo,$CollegeId);
		}else{
			$StudentId = $this->StudentModel->insert_entry($studentData);
		}
		if($StudentId>0){
			$itemStudentData = array(
				'CollegeId' => $CollegeId,
				'StudentAdmissionNo' => $AdmissionNo,
				'ItemId'	=> $ItemId,
				'ItemType'  => $ItemType
			);
			$isInsert = $this->StudentModel->insert_student_item_entry($itemStudentData);
			if($isInsert){
				$this->response([
					'resultCode'=>1,
					'message'=>'sucess',

				]);
			}else{
				$this->response([
					'resultCode'=>0,
					'message'=>'Error Occured .Please try again after sometime ',

				]);

			}
		}
	}


	public function test_get(){
		$result = $this->ItemsModel->get_all_student_item_single_by_college(40);
		$this->response([
				'resultCode'=>1,
				'message'=>'sucess',
				'data'=>$result
			]);
	}
	public function item_registered_participant_group_get(){
		$CollegeId = $this->get("collegeid");
		$ItemId = $this->get("itemid");

		$result = $this->ItemsModel->get_all_registered_participant_group($ItemId,$CollegeId);
		if(count($result)>0){
			$this->response([
				'resultCode'=>1,
				'message'=>'sucess',
				'data'=>$result,
				'count'=>count($result),
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'message'=>'No Result'
			]);
		}
	}

	public function get_all_students_by_group_item_get(){
		$CollegeId = $this->get("collegeid");
		$ItemId = $this->get("itemid");
		$result = $this->ItemsModel->get_all_registerd_student_deatils_by_group_item($ItemId,$CollegeId);
		$this->response([
				'resultCode'=>1,
				'data'=>$result,
			]);

	}

	public function delete_registerd_item_get($id){
		$result= $this->ItemsModel->delete_registerd_item($id);
		if($result){
			$this->response([
				'resultCode'=>1,
				'message'=>'Sucessfully Deleted'
			]);
		}else{
			$this->response([
				'resultCode'=>0,
				'message'=>'Error Occoured'
			]);
		}
	}
	
//Item Wise Report -Sumith

	public function fetch_college_details_by_item_get($id) {

		$result=$this->ItemsModel->get_all_registered_student($id);
		if ($result) {
			$this->response([
				'resultCode' => 1,
				'data'=>$result,
				'message' => 'Sucessfully',
			]);
		} else {
			$this->response([
				'resultCode' => 0,
				'message' => 'Error Occoured',
			]);
		}

		
	}

	public function get_itemwise_result_get($id){
		$result = $this->ResultModel->get_itemwise_result($id);
		if ($result) {
			$this->response([
				'resultCode' => 1,
				'data'=>$result,
				'message' => 'Sucessfully',
			]);
		} else {
			$this->response([
				'resultCode' => 0,
				'message' => 'Error Occoured',
			]);
		}

	}
}
	


	
