<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "/third_party/tcpdf/tcpdf.php";
class College extends CI_Controller {

	public function index()
	{
		
		$collegeId	  = $this->session->userdata('CollegeId');
		$CollegeDeatils= $this->CollegeModel->get_college_by_id($collegeId);
		$this->session->set_userdata('CollegeName', $CollegeDeatils[0]->CollegeName);
		$data=array(
			'userType'=>'college',
			'email'=>$this->session->userdata('email'),
			'menu'=>'dashboard',
			'allStudents'=>$this->StudentModel->get_all_student_by_college($collegeId),
			'allItems'=>$this->ItemsModel->get_all_registered_items_by_college($collegeId),
			'allCounts'=>$this->ItemsModel->get_all_items_and_count_by_college($collegeId),
			'allCountParticipant'=>$this->StudentModel->get_all_participant_by_college($collegeId),


		);
		$this->template->set('title', 'College DashBoard');
		$this->template->load('template', 'contents' , 'college/home', $data);
	}

	public function settings(){
		$data=array(
			'userType'=>'college',
			'email'=>$this->session->userdata('email'),
			'menu'=>'settings'
		);
		$this->template->set('title', 'Change Password');
		$this->template->load('template', 'contents' , 'college/change_password', $data);
	}
	public function report(){
		$collegeId	  = $this->session->userdata('CollegeId');
		$data=array(
			'userType'=>'college',
			'email'=>$this->session->userdata('email'),
			'allRegisteredStudent'=>$this->ItemsModel->get_all_student_item_single_by_college($collegeId),
			'menu'=>'report'
		);
		$this->template->set('title', 'Report');
		$this->template->load('template', 'contents' , 'college/report', $data);

	}
	public function students(){
		
		$registerType = $this->uri->segment(3);
		$collegeId	  = $this->session->userdata('CollegeId');
		$data=array(
			'userType'=>'college',
			'email'=>$this->session->userdata('email'),
			'menu'=>'students',
			'registerType'=>$registerType,
			'allStudents'=>$this->StudentModel->get_all_student_by_item_type($collegeId,$registerType),
			'collegeId'=>$collegeId,
			'allRegisterdGroupItem'=>$this->ItemsModel->get_all_registered_group_items_by_college($collegeId)
		);
		$this->template->set('title', 'Registred Student Details');
		$this->template->load('template', 'contents' , 'college/student_list', $data);
	}

	public function students_register_single(){
		
		$isInsert = $this->input->get("isInsert");
		$data=array(
			'userType'=>'college',
			'email'=>$this->session->userdata('email'),
			'menu'=>'students',
			'itemDeatils'=>$this->ItemsModel->get_all_items_by_type(2),
			'isInsert'=>$isInsert,
			'collegeId'=>$this->session->userdata('CollegeId'),



		);
		$this->template->set('title', 'Single Item Register');
		$this->template->load('template', 'contents' , 'college/student_register_single', $data);
	}
	public function students_register_group(){
		
		$isInsert = $this->input->get("isInsert");
		$data=array(
			'userType'=>'college',
			'email'=>$this->session->userdata('email'),
			'menu'=>'students',
			'itemDeatils'=>$this->ItemsModel->get_all_items_by_type(1),
			'isInsert'=>$isInsert,
			'collegeId'=>$this->session->userdata('CollegeId'),
			
		);
		$this->template->set('title', 'Group Item Register');
		$this->template->load('template', 'contents' , 'college/student_register_group', $data);
	}

	//Itemtype 1-Group,2-Single
	public function insert_students(){
		
		$ItemType  = $this->input->post("hdItemType");
		$CollegeId = $this->session->userdata('CollegeId');
		$ItemId    = $this->input->post("selItem");
		$AdmissionNo= $this->input->post("txtRegisterNo");
		$IsStudentExit = $this->input->post("hdIsStudentExit");
		$StudentId=0;
		$studentData = array(
			'AdmissionNo' 	=> $AdmissionNo,
			'Name' 			=> $this->input->post("txtStudentName"),
			'Class' 		=> $this->input->post("txtClass"),
			'Course' 		=> $this->input->post("txtCourse"),
			'CollegeId' 	=> $CollegeId,
			'PhoneNo' 		=> $this->input->post("txtMobileNo"),
			'EmailId' 		=> $this->input->post("txtStudentEmail")
		);

		if($ItemType==2){

			if(count($this->ItemsModel->item_register_rule_single_3($CollegeId,$AdmissionNo,$ItemId))>0){
				redirect('College/students_register_single?isInsert=2&msg=Student Already register in the same item');
			}
			
			elseif (count($this->ItemsModel->item_register_rule_single_2($CollegeId,$AdmissionNo))>=5) {
				redirect('College/students_register_single?isInsert=2&msg=Maximum number of individual items, allowed for one student to participate is strictly limited to 5 (five).');
			}elseif(count($this->ItemsModel->item_register_rule_single_1($ItemId,$CollegeId))>=1){
				redirect('College/students_register_single?isInsert=2&msg=Only a single student can participate in an individual item from a college');
			}
		}
		if(count($this->StudentModel->get_student_by_admission_college($CollegeId,$AdmissionNo))>0){
			$StudentId= $this->StudentModel->update_student($studentData,$AdmissionNo,$CollegeId);
		}else{
			$StudentId = $this->StudentModel->insert_entry($studentData);
		}
		
		if($StudentId>0){
			$itemStudentData = array(
				'CollegeId' => $CollegeId,
				'StudentAdmissionNo' => $AdmissionNo,
				'ItemId'	=> $ItemId,
				'ItemType'  => $ItemType
			);
			$isInsert = $this->StudentModel->insert_student_item_entry($itemStudentData);
			if($isInsert){
				if($ItemType==1){
					redirect('College/students_register_group?isInsert=1');
				}else if($ItemType==2){
					redirect('College/students_register_single?isInsert=1');
				}
			}else{
				if($ItemType==1){
					redirect('College/students_register_group?isInsert=2');
				}else if($ItemType==2){
					redirect('College/students_register_single?isInsert=2&msg=some error occour');
				}
			}

		}
	}

	    public function generatePDFFile() {
            $collegeId    = $this->session->userdata('CollegeId'); 
                     
            $htmlContent='';
            $singleItem = $this->ItemsModel->get_all_student_item_single_by_college($collegeId);
            $groupItemCount = $this->ItemsModel->get_all_stuent_group_item_count_by_college($collegeId);
            $countStudents = $this->StudentModel->get_all_participant_by_college($collegeId);
            $groupItems = $this->ItemsModel->get_all_stuent_group_item_by_college($collegeId);
            $countSingleItem = $this->ItemsModel->count_item_by_college($collegeId,2);
            $countGroupItem = $this->ItemsModel->count_item_by_college($collegeId,1);
            $data = array(
            	'StudentSigleItemDetails' =>$singleItem,
            	'CollegeDeatils'=>$this->CollegeModel->get_college_by_id($collegeId),
            	'TotalGroupItem'=>$groupItemCount,
            	'GroupItemCount'=>$countGroupItem,
            	'TotalSingleItem'=>$countSingleItem,
            	'TotalParticipant'=>count($countStudents),
            	'StudentGroupItemDetails'=>$groupItems,
            	'GetAllParticipantByClg'=>$this->ItemsModel->get_all_participants_by_college($collegeId),
            	'GetAllParticipantByClgSingleItem'=>$this->ItemsModel->get_all_participants_by_college_sigle_item($collegeId),
            	'CollegeId'=>$collegeId,
           	);
             
             $htmlContent = $this->load->view('college/report_file_dec', $data, TRUE); 
             $htmlContent2 = $this->load->view('college/report_file', $data, TRUE);       
             $createPDFFile = time().'.pdf';
            $this->createPDF($_SERVER['DOCUMENT_ROOT'].'/doc/pdf/'.$createPDFFile, $htmlContent,$htmlContent2);
             redirect(site_url().'doc/pdf/'.$createPDFFile);
            
         }

	 // create pdf file 
        public function createPDF($fileName,$html,$html2) {
            ob_start(); 
            // Include the main TCPDF library (search for installation path).
            $this->load->library('Pdf');
            // create new PDF document
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('minusbugs');
            $pdf->SetTitle('MG UNIVERSITY');
            $pdf->SetSubject('MAHATMA GANDHI UNIVERSITY Kalolsavam 2019');
            $pdf->SetKeywords('mg,minusbugs');

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            
			            // set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(true);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(20);
            $pdf->SetFooterMargin(15);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->SetAutoPageBreak(TRUE, 25);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }       

            // set font
            $pdf->SetFont('dejavusans', '', 10);

            // add a page
            $pdf->AddPage();
    
            // output the HTML content
            $pdf->writeHTML($html, true, false, true, false, '');
             $pdf->AddPage();
              $pdf->writeHTML($html2, true, false, true, false, '');

            // reset pointer to the last page
            $pdf->lastPage();       
            ob_end_clean();
            //Close and output PDF document
            $pdf->Output($fileName, 'F');        
        }



	
}

class MYPDF extends TCPDF {

	

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 15, 'Principal Seal                                                                                                                                         Principal Sign', 0, false, 'c', 0, '', 0, false, 'T', 'L');
	}
}