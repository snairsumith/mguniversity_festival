<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$data=array(
			'userType'=>'admin',
			'email'=>$this->session->userdata('email'),
			'allItems'=>$this->ItemsModel->get_all_items(),
			'allStudents'=>$this->StudentModel->get_all_participant(),
			'allCollege'=>$this->CollegeModel->get_all_college(),
			'menu'=>'home',
		);
		$this->template->set('title', 'Admin DashBoard');
		$this->template->load('template', 'contents' , 'admin/home', $data);
	}
	public function college(){
		
		$data=array(
			'userType'=>'admin',
			'allCollege'=>$this->CollegeModel->get_all_college(),
			'menu'=>'college',
		);
		$this->template->set('title', 'College Deatils');
		$this->template->load('template', 'contents' , 'admin/college', $data);
	}
	public function items(){
		$data=array(
			'userType'=>'admin',
			'allItems'=>$this->ItemsModel->get_all_items(),
			'menu'=>'items',
		);
		$this->template->set('title', 'Item Details');
		$this->template->load('template', 'contents' , 'admin/items', $data);
	}
	public function report(){
		$data=array(
			'userType'=>'admin',
			'menu'=>'report',
		);
		$this->template->set('title', 'Report');
		$this->template->load('template', 'contents' , 'admin/report', $data);
	}
	public function report_college()
	{
		$data=array(
			'userType'=>'admin',
			'menu'=>'report_clg',
			'allRegisterdCollege'=>$this->CollegeModel->get_all_registered_college(),
			'allParticipant'=>$this->ItemsModel->get_all_participants(),
		);
		$this->template->set('title', 'Report');
		$this->template->load('template', 'contents' , 'admin/report_college', $data);
	}
	public function report_item()
	{
		$data=array(
			'userType'=>'admin',
			'menu'=>'report_item',
			'allRegisterdItem'=>$this->ItemsModel->get_all_registered_item(),
			'allRegisterdColleg'=>$this->CollegeModel->get_all_registered_college_by_item(),
			'allRegisterdStudent'=>$this->StudentModel->get_all_participants_by_college_and_item(),
		);
		$this->template->set('title', 'Report');
		$this->template->load('template', 'contents' , 'admin/report_item', $data);
	}
	public function settings(){
		$data=array(
			'userType'=>'admin',
			'menu'=>'settings',
		);
		$this->template->set('title', 'Change Password');
		$this->template->load('template', 'contents' , 'admin/change_pwd', $data);
	}

	public function result_add(){
		$isInsert = $this->input->get("code");
		$data = array(
			'userType'=>'admin',
			'menu'=>'settings',
			'allItems'=>$this->ItemsModel->get_all_items(),
			'allCollege'=>$this->CollegeModel->get_all_college(),
			'code'=>$isInsert,

			

		);

		$this->template->set('title', 'Add Result');
		$this->template->load('template', 'contents' , 'admin/add_result', $data);
	}

	public function view_result(){
		
		$data = array(
			'userType'=>'admin',
			'menu'=>'view_result',
			'allResult'=>$this->ResultModel->get_all_result(),
		);

		$this->template->set('title', 'Add Result');
		$this->template->load('template', 'contents' , 'admin/view_result', $data);
	}

	public function result_action(){

		$itemId = $this->input->post("selectItem");
		$resultType= $this->input->post("selectType");
		$collegeId = $this->input->post("selectCollege");
		$admissionNo=$this->input->post("txtAdmissionNo");
		$point = 0;

		// redirect('College/students_register_single?isInsert=2&msg=Student Already register in the same item');

		//Chcek if this student Register on this item
		$isRegisterOrNot = $this->ItemsModel->item_register_rule_single_3($collegeId,$admissionNo,$itemId);
		// print_r($isRegisterOrNot);exit;
		if(count($isRegisterOrNot)>0){
			$resultItem = $this->ItemsModel->item_by_id($itemId);
			if($resultItem){
				$itemType= $resultItem[0]->ItemTypeId;
				if($itemType==1){
					echo "group";
					if($resultType=='F'){
						$point=10;
					}elseif ($resultType=='S') {
						$point=6;
					}elseif ($resultType=='T') {
						$point=2;
					}
				}else if($itemType==2){
					echo "Single";
					if($resultType=='F'){
						$point=5;
					}elseif ($resultType=='S') {
						$point=3;
					}elseif ($resultType=='T') {
						$point=1;
					}

				}
			}

			$data = array(
				'CollegeId' => $collegeId, 
				'AdmissionNo' => $admissionNo, 
				'ItemId' =>$itemId , 
				'ItemType'=>$itemType,
				'ResultType' =>$resultType, 
				'Point' => $point, 

			);

			// check_is_already_enter($ItemId,$collegeId,$admissionNo,$ResultType)
			$isInsertAllredy = $this->ResultModel->check_is_already_enter($itemId,$collegeId,$admissionNo,$resultType);
			// print_r($isInsertAllredy);exit;
			if(count($isInsertAllredy)>0){
				redirect('Admin/result_add?code=4');
			}else{
			$result = $this->ResultModel->insert_result($data);

			if($result>0){

				$resultCollege = $this->CollegeModel->get_college_by_id($collegeId);
				if($resultCollege){
					$CollegeTotalPoint = $resultCollege[0]->TotalPoint;
					$collegeFinalPoint = $CollegeTotalPoint+$point;
					$this->ResultModel->update_college_point($collegeFinalPoint,$collegeId);
				}
				
				if($itemType==2){
					$studentDetails = $this->StudentModel->get_student_by_admission_college($collegeId,$admissionNo);
					if($studentDetails){
						$TotalPoint = $studentDetails[0]->TotalPoint;
						$finalPoint= $TotalPoint+$point;
						$resultUpdateStudentPoint = $this->ResultModel->update_student_point($admissionNo,$collegeId,$finalPoint);

					}
				}
				redirect('Admin/result_add?code=1');
			}else{
				redirect('Admin/result_add?code=3');
			}
		}
		}else{
			redirect('Admin/result_add?code=2');
		}

		// echo 'ITM:'.$itemId.'Res:-'.$resultType.'-Clg:-'.$collegeId.'adm:-'.$admissionNo;


	}

		public function generatePDFFile() {
            
            $htmlContent='';
            
            $data = array(
            	'allRegisterdCollege'=>$this->CollegeModel->get_all_registered_college(),
            	'allParticipant'=>$this->ItemsModel->get_all_participants(),
           	);
 
        
             $htmlContent = $this->load->view('admin/report_file_college_wise', $data, TRUE); 
             $htmlContent2 = $this->load->view('admin/report_file_college_wise', $data, TRUE); 
             $createPDFFile = time().'.pdf';
             $this->createPDF($_SERVER['DOCUMENT_ROOT'].'/doc/pdf/'.$createPDFFile, $htmlContent,$htmlContent2);
             redirect(site_url().'doc/pdf/'.$createPDFFile);
            
         }

		public function generatePDFFileItem() {
            
          $htmlContent = '';
		$ItemId = $this->input->post("selectItem");
		$itemResult = $this->ItemsModel->item_by_id($ItemId);

		$data = array(
			'ItemDetails' => $this->ItemsModel->item_by_id($ItemId),
			'allRegisterdStudent' => $this->ItemsModel->get_all_registered_student($ItemId),
			'collegeDeatils'=>$this->ItemsModel->get_all_college_by_itemid($ItemId),


		);

		
		// $this->load->view('admin/report_file_item_wise', $data);
		if($itemResult[0]->ItemTypeId==1){
			$this->load->view('admin/report_file_item_wise', $data);
		}else{
			// $htmlContent2 = $this->load->view('admin/report_file_item_wise', $data, TRUE);
		 // $createPDFFile = time() . '.pdf';
		 // $this->createPDF($_SERVER['DOCUMENT_ROOT'] . '/doc/pdf/' . $createPDFFile, $htmlContent2);
		 // redirect(site_url() . 'doc/pdf/' . $createPDFFile);
			$this->load->view('admin/report_file_item_wise', $data);
		}
		
            
         }
          public function createPDF($fileName,$html2) {
            ob_start(); 
            // Include the main TCPDF library (search for installation path).
            $this->load->library('Pdf');
            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('minusbugs');
            $pdf->SetTitle('MG UNIVERSITY');
            $pdf->SetSubject('MAHATMA GANDHI UNIVERSITY Kalolsavam 2019');
            $pdf->SetKeywords('mg,minusbugs');

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            
			            // set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(true);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(20);
            $pdf->SetFooterMargin(15);

            // set auto page breaks
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->SetAutoPageBreak(TRUE, 25);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }       

            // set font
            $pdf->SetFont('dejavusans', '', 10);

            // add a page
            $pdf->AddPage();
    
            // output the HTML content
            // $pdf->writeHTML($html, true, false, true, false, '');
            //  $pdf->AddPage();
              $pdf->writeHTML($html2, true, false, true, false, '');

            // reset pointer to the last page
            $pdf->lastPage();       
            ob_end_clean();
            //Close and output PDF document
            $pdf->Output($fileName, 'F');        
        }

        public function schedule_item(){

        	$data = array(
			'userType'=>'admin',
			'menu'=>'add_schedule',
			'allItems'=>$this->ItemsModel->get_all_items(),
			'allSchedule'=>$this->ItemsModel->get_all_schedule(),
		);

		$this->template->set('title', 'Schedule Stage');
		$this->template->load('template', 'contents' , 'admin/add_schedule', $data);
        	
        	
        }

        public function schedule_action(){
        	$day=$this->input->post("selDay");
        	$stage=$this->input->post("selStage");
        	$item=$this->input->post("selectItem");
        	$time=$this->input->post("txtTime");
        	$data = array(
        		'ItemId' => $item, 
        		'Day' => $day, 
        		'Stage' => $stage, 
        		'Time' => $time, 
        		
        	);

        	$result = $this->ItemsModel->insert_schedule($data);
        	redirect('Admin/schedule_item?code=1');
        }

	
}
