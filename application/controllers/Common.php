<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	public function index()
	{
		$this->load->view('common/home');
	}

	public function login(){
		$data = array('isLogin' => 0, );
		$this->load->view('common/login',$data);
	}

	public function login_action(){
		$email = $this->input->post("txtEmail");
		$pwd   = $this->input->post("txtPassword");
		$resultData = $this->AccountModel->login_check($email,$pwd);
		if(count($resultData)>0){
			$this->session->set_userdata('email', $email);
			if($resultData[0]->RoleType==1){
				redirect('Admin/index');
			}else if($resultData[0]->RoleType==2){
				$this->session->set_userdata('CollegeId', $resultData[0]->CollegeId);
				redirect('College/index');
				// $data = array('isLogin' => 3, );
				// $this->load->view('common/login',$data);
				
			}
			
			
			
			
		}else{
			$data = array('isLogin' => 2, );
			$this->load->view('common/login',$data);
		}
	}
	public function instruction(){
		$this->load->view('common/instruction');
	}
	public function rules(){
		$this->load->view('common/rules');
	}
	public function schedule(){
		$data['allSchedule']=$this->ItemsModel->get_all_schedule();
		$this->load->view('common/schedule',$data);
	}
	public function result_college_public(){
		$top_ten_college =$this->ResultModel->get_top_10_college();
		$data = array(
			'topCollege' => $top_ten_college
		 );
		$this->load->view('common/result_college_for_display',$data);
	
	}
	public function result(){
		$top_ten_college =$this->ResultModel->get_top_college();
		$data = array(
			'topCollege' => $top_ten_college,
			'allItems'=>$this->ItemsModel->get_all_items(),
		 );
		$this->load->view('common/college_result',$data);
	
	}
	public function item_result(){
		$top_ten_college =$this->ResultModel->get_top_10_college();
		$data = array(
			'topCollege' => $top_ten_college,
			'allItems'=>$this->ItemsModel->get_all_items(),
		 );
		$this->load->view('common/item_result',$data);
	}
	public function individual_result(){
		$top_students =$this->ResultModel->get_top_students();
		$data = array(
			'allStudents' => $top_students
			
		 );
		$this->load->view('common/individual_result',$data);
	}
	public function result_by_student(){
		$arrayName = array();
		$data["result"]=$arrayName;
		$this->load->view('common/result_student',$data);
		
	}
	public function result_by_student_action(){
		$admno=$this->input->post("txtStudentAdmissionNo");
		$result = $this->ResultModel->get_result_by_student($admno);
		$data["result"]=$result;
		$this->load->view('common/result_student',$data);
		
	}
	public function logout(){

		$this->session->sess_destroy();
		$data = array('isLogin' => 0, );
		$this->load->view('common/login',$data);
	}
	
}