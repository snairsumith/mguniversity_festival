<?php
class Student_model extends CI_Model {

	public function get_all_items() {
		$query = $this->db->get('item');
		return $query->result();
	}
	public function get_all_student_by_item_type($CollegeId, $ItemType) {
		if ($ItemType == "single") {
			$ItemType = 2;
		} else {
			$ItemType = 1;
		}

		$this->db->select('student.*');
		$this->db->from(' student');
		$this->db->join('students_item', 'student.AdmissionNo=students_item.StudentAdmissionNo');
		$this->db->where('students_item.CollegeId', $CollegeId);
		$this->db->where('students_item.ItemType', $ItemType);
		$this->db->group_by("student.AdmissionNo");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_all_student_by_college($CollegeId) {

		$this->db->select('student.*');
		$this->db->from(' student');
		$this->db->where('student.CollegeId', $CollegeId);
		$this->db->group_by("student.AdmissionNo");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_all_participant_by_college($CollegeId){
		$this->db->distinct();
		$this->db->select('students_item.StudentAdmissionNo');
		$this->db->from(' students_item');
		$this->db->where('students_item.CollegeId', $CollegeId);
		$this->db->group_by("students_item.StudentAdmissionNo");
		$query = $this->db->get();
		return $query->result();
	}
		public function get_all_participant(){
		$this->db->distinct();
		$this->db->select('students_item.StudentAdmissionNo');
		$this->db->from(' students_item');
		$this->db->group_by("students_item.StudentAdmissionNo");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_all_student() {

		$this->db->select('student.*');
		$this->db->from(' student');
		$this->db->join('students_item', 'student.AdmissionNo=students_item.StudentAdmissionNo');
		$this->db->group_by("student.AdmissionNo");
		$query = $this->db->get();
		return $query->result();
	}

	public function insert_entry($data) {
		$this->db->insert('student', $data);
		return $this->db->insert_id();
	}

	public function insert_student_item_entry($data) {
		return $this->db->insert('students_item', $data);

	}

	public function update_entry() {

	}
	public function get_student_by_admission_college($CollegeId,$AdmissionNo){
		$this->db->select('*');
		$this->db->from(' student');
		$this->db->where('student.AdmissionNo',$AdmissionNo);
		$this->db->where('student.CollegeId',$CollegeId);
		$this->db->limit(1);
		$query = $this->db->get();
		 // echo $this->db->last_query();exit;
		return $query->result();
	}
	function get_all_participants_by_college_and_item(){
	$this->db->select('college.collegeId,college.CollegeName, item.ItemName,item.ItemId,item.ItemTypeId,student.Name,student.AdmissionNo,student.EmailId,student.PhoneNo,student.Class,student.Course,student.CollegeId');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	$this->db->join('college','students_item.CollegeId=college.CollegeId');
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}
function update_student($data,$admno,$clgid){
	$this->db->where('AdmissionNo',$admno);
	$this->db->where('CollegeId',$clgid);
	$res=$this->db->update('student',$data);
	return $res;
}
}
?>