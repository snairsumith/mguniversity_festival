<?php
class Result_model extends CI_Model {
	
	public function insert_result($data){
		
		$this->db->insert('result_tbl',$data);
        return $this->db->insert_id();
	}

	// public function update_college_point($point,$collegeId){
	// 	$this->db->set('TotalPoint','TotalPoint+$point'',false);
	// 	$this->db->where('CollegeId',$collegeId);
	// 	return $this->db->update('college');
	// } 

	public function update_student_point($admissionNo,$collegeId,$finalPoint){

		$this->db->set('TotalPoint',$finalPoint,false);
		$this->db->where('CollegeId',$collegeId);
		$this->db->where('AdmissionNo',$admissionNo);
		return $this->db->update('student');
	}

	public function update_college_point($point,$collegeId){
		$this->db->set('TotalPoint',$point,false);
		$this->db->where('CollegeId',$collegeId);
		return $this->db->update('college');

	}

	public function get_all_result(){
		$this->db->select('student.Name,college.CollegeName,item.ItemName,student.AdmissionNo,result_tbl.Point,result_tbl.ResultType');
		$this->db->from('result_tbl');
		$this->db->join('student','student.AdmissionNo=result_tbl.AdmissionNo');
		$this->db->join('college','college.CollegeId=result_tbl.CollegeId');
		$this->db->join('item','item.ItemId=result_tbl.ItemId');
		$query = $this->db->get();
		return $query->result();

	}

	public function get_top_10_college(){
		$this->db->select('CollegeName,TotalPoint');
		$this->db->from('college');
		$this->db->order_by('TotalPoint','desc');
		$this->db->limit(10, 0);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_top_college(){
		$this->db->select('CollegeName,TotalPoint');
		$this->db->from('college');
		$this->db->where('TotalPoint>0');
		$this->db->order_by('TotalPoint','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_top_students(){
		// $this->db->select('student.Name,student.AdmissionNo,college.CollegeName,student.TotalPoint,student.CollegeId');
		// $this->db->from('student');
		// $this->db->join('college','student.CollegeId=college.CollegeId');
		// $this->db->where('student.TotalPoint>0');
		// $this->db->order_by('student.TotalPoint','desc');
		// $query = $this->db->get();
		// return $query->result();
				$sql="SELECT DISTINCT student.Name,college.CollegeName,student.AdmissionNo as adm,
(select sum(Point) FROM result_tbl WHERE AdmissionNo=adm)as TotalPoint
FROM student INNER JOIN college on college.CollegeId=student.CollegeId
INNER JOIN result_tbl on result_tbl.AdmissionNo=student.AdmissionNo
where result_tbl.ItemType=2 order by TotalPoint DESC";
		$query = $this->db->query($sql);
		return  $query->result();

	}

	public function get_itemwise_result($id){
		$this->db->select('student.Name,student.AdmissionNo,college.CollegeName,result_tbl.ResultType,result_tbl.ItemType');
		$this->db->from('result_tbl');
		$this->db->join('student','student.AdmissionNo=result_tbl.AdmissionNo');
		$this->db->join('college','college.CollegeId=result_tbl.CollegeId');
		$this->db->where('result_tbl.ItemId',$id);
		$this->db->order_by('result_tbl.ResultType','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function check_is_already_enter($ItemId,$collegeId,$admissionNo,$ResultType){

		// echo "haiii";exit;
		$this->db->select('*');
		$this->db->from('result_tbl');
		$this->db->where('CollegeId',$collegeId);
		$this->db->where('AdmissionNo',$admissionNo);
		$this->db->where('ItemId',$ItemId);
		$this->db->where('ResultType',$ResultType);
	
		$query = $this->db->get();
			// echo $this->db->last_query();exit;
		return $query->result();

	}

 public function get_result_by_student($admissionNo){

 	$this->db->select('item.ItemName,result_tbl.ResultType,result_tbl.Point');
 	$this->db->from('result_tbl');
 	$this->db->join('item','result_tbl.ItemId=item.ItemId');
 	$this->db->where('result_tbl.AdmissionNo',$admissionNo);
 	$query = $this->db->get();
 	return $query->result();
 }
 

}
?>