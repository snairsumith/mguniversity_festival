<?php
class College_model extends CI_Model {

      

        public function get_all_college()
        {       

                $this->db->select('*');
                $this->db->order_by('CollegeId','desc');
                $query =$this->db->get('college');
                return $query->result();
        }
        public function get_college_by_mail($email){
                $this->db->select('*');
                $this->db->where('EmailId',$email);
                $query =$this->db->get('college');
                return $query->result();
        }

        public function get_college_by_id($id){
                $this->db->select('*');
                $this->db->where('CollegeId',$id);
                $query =$this->db->get('college');
                return $query->result();

        }

        public function insert_entry($data)
        {
               $this->db->insert('college',$data);
               return $this->db->insert_id();
        }

        public function update_entry($college_data,$id,$login_data)
        {
            $this->db->where('CollegeId',$id);
            $res=$this->db->update('college',$college_data);
            $this->db->where('CollegeId',$id);
            $res2=$this->db->update('login',$login_data);
           if($res>0&&$res2>0){
            return 1;
           }else{
            return 0;
           }
        }

        public function get_all_registered_college(){

                $this->db->distinct();
                $this->db->select('students_item.CollegeId,college.CollegeName,college.Address');
                $this->db->from('students_item');
                $this->db->join('college','college.CollegeId=students_item.CollegeId');
                $query =$this->db->get();
                return $query->result();
        }

        public function get_all_registered_college_by_item(){

                $this->db->distinct();
                $this->db->select('students_item.CollegeId,college.CollegeName,college.Address,item.ItemId,item.ItemName');
                $this->db->from('students_item');
                $this->db->join('college','college.CollegeId=students_item.CollegeId');
                $this->db->join('item','item.ItemId=students_item.ItemId');
                
                $query =$this->db->get();
                return $query->result();
        }
   
}
?>