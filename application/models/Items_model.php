<?php
class Items_model extends CI_Model {

	public function get_all_items() {
		$query = $this->db->get('item');
		return $query->result();
	}
	public function get_all_items_by_type($TypeId) {
		$this->db->where('ItemTypeId', $TypeId);
		$query = $this->db->get('item');
		return $query->result();
	}
	public function get_all_items_by_college($CollegeId) {
		$this->db->where('ItemTypeId', $TypeId);
		$query = $this->db->get('item');
		return $query->result();
	}

	public function get_all_items_by_type_student($TypeId, $StudentId,$CollegeId) {
		$this->db->select('item.ItemName,students_item.StudentItemId,students_item.CreatedDate');
		$this->db->from('students_item');
		$this->db->join('item', 'item.ItemId=students_item.ItemId');
		$this->db->where('students_item.ItemType', $TypeId);
		$this->db->where('students_item.StudentAdmissionNo', $StudentId);
		$this->db->where('students_item.CollegeId', $CollegeId);
		$query = $this->db->get();
		return $query->result();
	}
	public function insert_entry($data) {

		return $this->db->insert('item', $data);

	}

	public function update_entry() {

	}
	


public function get_all_registered_items_by_college($CollegeId) {
	$this->db->distinct();
	$this->db->select('students_item.ItemId');
	$this->db->from('students_item');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$query = $this->db->get();
	return $query->result();
}

public function get_all_registered_group_items_by_college($CollegeId) {
	$this->db->distinct();
	$this->db->select('students_item.ItemId,item.ItemName,item.ItemDescription');
	$this->db->from('students_item');
	$this->db->join('item','item.ItemId=students_item.ItemId');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$this->db->where('students_item.ItemType', 1);
	$query = $this->db->get();
	return $query->result();
}

public function get_all_items_and_count_by_college($CollegeId) {
	$this->db->select('students_item.ItemId,item.ItemName,COUNT(*) as "ItemCount" ');
	$this->db->from('students_item');
	$this->db->JOIN('item', 'item.ItemId=students_item.ItemId');
	$this->db->JOIN('student', 'student.AdmissionNo=students_item.StudentAdmissionNo');
	$this->db->where('student.CollegeId', $CollegeId);
	$this->db->GROUP_BY('students_item.ItemId');
	$query = $this->db->get();
	return $query->result();
}

public function delete_items_by_type_item($StudentId) {
	$this->db->delete(' * ');
	$this->db->from('students_item');
	$this->db->where('StudentId', $StudentId);
	$query = $this->db->get();
	return $query->result();
}


//Rules in Sigle Item
//Register Only two students in 1 item
public function item_register_rule_single_1($itemId,$collegeId){
	$this->db->select('students_item.ItemId');
	$this->db->from('students_item');
	$this->db->where('students_item.CollegeId', $collegeId);
	$this->db->where('students_item.ItemId', $itemId);
	$query = $this->db->get();
	return $query->result();


}
//Maximum number of individual items, allowed for one student to participate is  limited to 5 . 
public function item_register_rule_single_2($collegeId,$admNo){
	$this->db->select('students_item.ItemId');
	$this->db->from('students_item');
	$this->db->where('students_item.CollegeId', $collegeId);
	$this->db->where('students_item.StudentAdmissionNo', $admNo);
	$this->db->where('students_item.ItemType',2);
	$query = $this->db->get();
	return $query->result();
}
//Same Item Register allredy
public function item_register_rule_single_3($collegeId,$admNo,$itemId){
	$this->db->select('students_item.ItemId');
	$this->db->from('students_item');
	$this->db->where('students_item.CollegeId', $collegeId);
	$this->db->where('students_item.StudentAdmissionNo', $admNo);
	$this->db->where('students_item.ItemId', $itemId);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}
//Maximum number of group items, each college is 6 . 
public function item_register_rule_group_1($collegeId){
	
	$this->db->select('*');
	$this->db->from('college_item_group');
	$this->db->where('CollegeId', $collegeId);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}


function item_by_id($itemId){
	$this->db->select('*');
	$this->db->from('item');
	$this->db->where('ItemId', $itemId);
	$query = $this->db->get();
	return $query->result();
}
function get_all_registered_participant_group($itemId,$CollegeId){
	$this->db->select('*');
	$this->db->from('students_item');
	$this->db->where('ItemId', $itemId);
	$this->db->where('CollegeId', $CollegeId);
	$query = $this->db->get();
	return $query->result();
}
//For checking group item regitserd count
function insert_group_inividual_entry_group($ItemId,$CollegeId){
	$query = $this->db->get_where('college_item_group', array(
            'ItemId' => $ItemId,
            'CollegeId'=>$CollegeId
        ));
	$count = $query->num_rows();
	if($count===0){
		$data = array(
			'ItemId' =>$ItemId ,
			'CollegeId'=>$CollegeId
		);
		$this->db->insert('college_item_group', $data);
	}

}

function get_all_registerd_student_deatils_by_group_item($ItemId,$CollegeId){
		$this->db->distinct();
	$this->db->select('students_item.StudentItemId,student.Name,student.AdmissionNo,student.PhoneNo,student.EmailId,student.Class,student.course,student.CollegeId');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->where('students_item.ItemId', $ItemId);
	$this->db->where('students_item.CollegeId', $CollegeId);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}

function get_all_student_item_single_by_college($CollegeId){

	$this->db->select('item.ItemId,item.ItemName,student.Name,student.AdmissionNo,student.EmailId,student.PhoneNo,student.Class,student.Course');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$this->db->where('students_item.ItemType', 2);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}
public function count_item_by_college($CollegeId,$ItemType){
	$this->db->distinct();
	$this->db->select('students_item.ItemId,item.ItemName');
	$this->db->from('students_item');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$this->db->where('students_item.ItemType', $ItemType);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}


function delete_registerd_item($StudentItemId){

	$this->db->select('*');
	$this->db->from('students_item');
	$this->db->where('students_item.StudentItemId', $StudentItemId);
	$query = $this->db->get();
	$result= $query->result();

	$ItemId = $result[0]->ItemId;
	$CollegeId = $result[0]->CollegeId;
	$ItemType = $result[0]->ItemType;
	if($ItemType==1){
		$this->db->select('*');
		$this->db->from('students_item');
		$this->db->where('students_item.ItemId', $ItemId);
		$this->db->where('students_item.CollegeId',$CollegeId);
		$query1 = $this->db->get();
		$result1= $query1->result();
		if(count($result1)==1){
			$this->db->where('ItemId', $ItemId);
			$this->db->where('CollegeId', $CollegeId);
			$this->db->delete('college_item_group');
		}
	}
	
	$this->db->where('StudentItemId', $StudentItemId);
	return $this->db->delete('students_item');
}

function get_all_stuent_group_item_by_college($CollegeId){
	$this->db->select('item.ItemName,item.ItemId,student.Name,student.AdmissionNo,student.EmailId,student.PhoneNo,student.Class,student.Course,student.CollegeId');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$this->db->where('students_item.ItemType', 1);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}

function get_all_stuent_group_item_count_by_college($CollegeId){
	$this->db->select('item.ItemName,item.Members,item.ItemId');
	$this->db->from('item');
	$this->db->join('college_item_group','college_item_group.ItemId=item.ItemId');
	$this->db->where('college_item_group.CollegeId',$CollegeId);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();

}

function get_all_participants_by_college($CollegeId){
	$this->db->select('item.ItemName,item.ItemId,student.Name,student.AdmissionNo,student.EmailId,student.PhoneNo,student.Class,student.Course');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}
function get_all_participants_by_college_sigle_item($CollegeId){
	$this->db->select('item.ItemName,item.ItemId,student.Name,student.AdmissionNo,student.EmailId,student.PhoneNo,student.Class,student.Course,student.CollegeId');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	$this->db->where('students_item.CollegeId', $CollegeId);
	$this->db->where('students_item.ItemType', 2);
	

	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}


function get_all_participants(){
	$this->db->select('item.ItemName,item.ItemId,item.ItemTypeId,student.Name,student.AdmissionNo,student.EmailId,student.PhoneNo,student.Class,student.Course,student.CollegeId');
	$this->db->from('students_item');
	$this->db->join('student','students_item.StudentAdmissionNo=student.AdmissionNo');
	$this->db->join('item','students_item.ItemId=item.ItemId');
	
	$query = $this->db->get();
	// echo $this->db->last_query();exit;
	return $query->result();
}

public function get_all_registered_item(){

     $this->db->distinct();
                $this->db->select('students_item.ItemId,item.ItemName');
                $this->db->from('students_item');
                $this->db->join('item','item.ItemId=students_item.ItemId');
                $query =$this->db->get();
                return $query->result();
        }


//Report Item wise Page -Sumith ( evide nigalude item wise  students edukan ulla code nigal ezuthanam)
  function get_all_registered_student($itemId){

    $this->db->select("
    	student.Name,
    	student.AdmissionNo,
    	student.Class,
    	student.Course,
    	student.PhoneNo,
    	student.EmailId,
    	college.CollegeName,
    	college.CollegeId"

    );
    $this->db->from('student');
    $this->db->join('students_item','student.AdmissionNo=students_item.StudentAdmissionNo');
    $this->db->join('college','college.CollegeId=student.CollegeId');
    $this->db->where('students_item.ItemId',$itemId);
    $this->db->order_by('students_item.CollegeId','asc');
    $query =$this->db->get();
    return $query->result();
}

function get_all_college_by_itemid($itemId){
	$this->db->distinct();
	$this->db->select('
		students_item.CollegeId,
		college.CollegeName'
	);
	$this->db->from('students_item');
	$this->db->join('college','students_item.CollegeId=college.CollegeId');
	$this->db->where('students_item.ItemId',$itemId);
	$query =$this->db->get();
    return $query->result();

}

function insert_schedule($data){
	return $this->db->insert('schedule',$data);
}

function get_all_schedule(){
	$this->db->select("schedule.Day,schedule.Stage,schedule.Time,schedule.CreatedOn,item.ItemName");
	$this->db->from("schedule");
	$this->db->join("item","item.ItemId=schedule.ItemId");
	$query =$this->db->get();
    return $query->result();

}

}
?>