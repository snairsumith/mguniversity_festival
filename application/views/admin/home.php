<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-lg-4">
				<div class="widget style1 navy-bg">
					<div class="row">
						<div class="col-4 text-center">
							<i class="fa fa-trophy fa-5x"></i>
						</div>
						<div class="col-8 text-right">
							<span>Total Items </span>
							<h2 class="font-bold"><?php echo count($allItems) ?></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="widget style1 lazur-bg">
					<div class="row">
						<div class="col-4">
							<i class="fa fa-child fa-5x"></i>
						</div>
						<div class="col-8 text-right">
							<span> Total Participants </span>
							<h2 class="font-bold"><?php echo count($allStudents) ?></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="widget style1 lazur-bg">
					<div class="row">
						<div class="col-4">
							<i class="fa fa-university fa-5x"></i>
						</div>
						<div class="col-8 text-right">
							<span> Total College </span>
							<h2 class="font-bold"><?php echo count($allCollege) ?></h2>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>College List</h5>
			</div>
			<div class="ibox-content">
				<table class="table">
					<thead>
						<tr>
						
						<th>College Name</th>
						<th>Address</th>
						<th>Email Id</th>
						<th>Phone No</th>
						<th>Mobile No</th>
						<th>Created Date</th>
					</tr>
					</thead>
					<tbody >
					<tr id="tbody_college"></tr>
					<?php $i=0; foreach ($allCollege as $key ) {  $i=$i+1;?>
					<tr>
						<td><?php echo $key->CollegeName ?></td>
						<td><?php echo $key->Address ?></td>
						<td><?php echo $key->EmailId ?></td>
						<td><?php echo $key->PhoneNo ?></td>
						<td><?php echo $key->MobileNo ?></td>
						<td><?php echo $key->CreatedDate ?></td>
					</tr>
						<?php if($i>10){break;}	} ?>
				</tbody>
			</table>
		</div>
	</div>

</div>
</div>