<div class="col-md-12">

	<div class="ibox">


        <button class="btn btn-primary pull-right mb-2" type="button"><i class="fa fa-plus"></i>&nbsp;Add New</button>
		<div class="ibox-title">
			

			<h5>Item List</h5>

		</div>
		<div class="ibox-content">
			<table class="table">
				<thead>
					<tr>
						
						<th>Item Name</th>
						<th>Item Description</th>
						<th>Item Type</th>
						<th>Created Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($allItems as $key ) { ?>
						<tr>
							<td><?php echo $key->ItemName ?></td>
							<td><?php echo $key->ItemDescription ?></td>
							<td><?php if($key->ItemTypeId==1){echo "Group Item ";}else{ echo "Single Item";} ?></td>
							<td><?php echo $key->CreatedDate ?></td>
							
					<?php	} ?>
					
				</tbody>
			</table>
			
		</div>
	</div>
</div>