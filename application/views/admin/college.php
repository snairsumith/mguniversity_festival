<div class="col-md-12">
	<div class="ibox">
		<button class="btn btn-primary pull-right mb-2" type="button" data-toggle="modal" data-target="#collegeAddModel"><i class="fa fa-plus"></i>&nbsp;Add New</button>
		<div class="ibox-title">
			
			<h5>College List</h5>
		</div>
		<div class="ibox-content">
			<table class="table">
				<thead>
					<tr>
						
						<th>College Name</th>
						<th>Address</th>
						<th>Email Id</th>
						<th>Phone No</th>
						<th>Mobile No</th>
						<th>Password</th>
						<th></th>
					</tr>
				</thead>
				<tbody >
					<tr id="tbody_college"></tr>
					<?php foreach ($allCollege as $key ) { ?>
					<tr>
						<td><?php echo $key->CollegeName ?></td>
						<td><?php echo $key->Address ?></td>
						<td><?php echo $key->EmailId ?></td>
						<td><?php echo $key->PhoneNo ?></td>
						<td><?php echo $key->MobileNo ?></td>
						<td><?php echo $key->Password ?></td>
						<td>
							<button class="btn-info btn btn-xs" data-toggle="modal" data-target="#collegeEditModel" onclick="fetchCollegeDeatild(<?php echo $key->CollegeId ?>)">Edit</button>
						</td>
					</tr>
						<?php	} ?>
						
					</tbody>
				</table>
				
			</div>
		</div>
	</div>

	<!-- insert college model  -->
	<div class="modal inmodal" id="collegeAddModel" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					
					<h4 class="modal-title">Add College Deatils</h4>
					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>College Name *</label>
								
								<input type="text" id="txtCollegeName" placeholder="Enter College Name" class="form-control ">
								<label id="cllegeName-error" class="error" ></label>
							</div>
							
							
							
							<div class="form-group">
								<label>Phone No</label>
								<input type="text" id="txtPhoneNo" placeholder="Enter Phone No" class="form-control">
							</div>
							<div class="form-group">
								<label>Mobile No</label>
								<input type="text" id="txtMobileNo" placeholder="Enter mobile no" class="form-control">
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea class="form-control" id="txtAddress"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Email /Username *</label>
								
								<input type="email" placeholder="Enter email" id="txtEmail" class="form-control">
								<label id="cllegeEmail-error" class="error" ></label>

							</div>
							<div class="form-group">
								<label>Password *</label>
								<input type="text" id="txtPassword" readonly="" class="form-control">
								<label id="cllegePwd-error" class="error" ></label>
							</div>
							<div class="form-group">
								<label>Conform Password</label>
								<input type="text" id="txtCPassword" readonly="" class="form-control">
								<label id="cllegeCPwd-error" class="error"></label>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="btnCollegeSave">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<!-- edit college model  -->

		<div class="modal inmodal" id="collegeEditModel" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					
					<h4 class="modal-title">Edit College Deatils</h4>
					
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>College Name *</label>
								
								<input type="text" id="txtEditCollegeName" placeholder="Enter College Name" class="form-control ">
								<label id="EditcllegeName-error" class="error" ></label>
							</div>
							
							
							
							<div class="form-group">
								<label>Phone No</label>
								<input type="text" id="txtEditPhoneNo" placeholder="Enter Phone No" class="form-control">
							</div>
							<div class="form-group">
								<label>Mobile No</label>
								<input type="text" id="txtEditMobileNo" placeholder="Enter mobile no" class="form-control">
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea class="form-control" id="txtEditAddress"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Email /Username *</label>
								
								<input type="email" placeholder="Enter email" id="txtEditEmail" class="form-control">
								<label id="EditcllegeEmail-error" class="error" ></label>

							</div>
							<div class="form-group">
								<label>Password *</label>
								<input type="text" id="txtEditPassword"  class="form-control">
								<label id="EditcllegePwd-error" class="error" ></label>
							</div>
							<div class="form-group">
								<label>Conform Password</label>
								<input type="text" id="txtEditCPassword"  class="form-control">
								<label id="EditcllegeCPwd-error" class="error"></label>
							</div>
							<input type="hidden" name="hdCollegeId" id="hdCollegeId" value="">
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="btnCollegeEdit">Update</button>
				</div>
			</div>
		</div>
	</div>