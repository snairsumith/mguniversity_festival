
<form action="generatePDFFileItem" class="col-md-12" method="POST">

<div class="col-md-12">
	<div class="ibox">
		<button class="btn btn-primary pull-right mb-2" type="submit"><i class="fa fa-plus"></i>&nbsp;Genarate Report</button>
		<div class="ibox-title">
			
			<h5>Item Wise Student Deatils</h5>
		</div>
		<div class="ibox-content">
			<div class="row">
				<div class="col-md-12">

					
				<select data-placeholder="Choose a item..." class="chosen-select" id="selectItem" name="selectItem"  tabindex="2" name="selItem" onchange="get_all_registerd_participant_by_item(this.value)" >
						<?php
						foreach ($allRegisterdItem as $key) {
						echo '<option value="' . $key->ItemId . '">' . $key->ItemName . '</option>';
						}
						?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="StudentDeatilsBind">
						
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

</form>

</div>
</div>