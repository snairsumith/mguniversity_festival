
	<div class="col-md-12">
		<div class="ibox">
			
			<div class="ibox-title">
				
				<h5>Add Result Here</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-md-12">
				<?php if($code==1){ ?>
					<div class="alert alert-success alert-dismissable">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<a class="alert-link" href="#">Sucess:</a>insert sucessfully  .
					</div>
				<?php }elseif ($code==2) { ?>
					<div class="alert alert-danger">
                                <a class="alert-link" href="#">Faill:</a> This student not registered in the selected item
                            </div>
				<?php  ?>
				<?php }elseif ($code==3) { ?>
					<div class="alert alert-danger">
                                <a class="alert-link" href="#">Faill:</a> Some error occourd pls try again later
                            </div>
				<?php }elseif ($code==4) { ?>
					<div class="alert alert-danger">
                                <a class="alert-link" href="#">Faill:</a> Already Inserted
                            </div>
				<?php } ?>

				</div>
					<div class="col-md-12">
						<?php echo form_open('Admin/result_action'); ?>
						<div class="form-group">
							<label>Select Item</label>
							<select data-placeholder="Choose a item..." class="chosen-select" id="selectItem" name="selectItem"  tabindex="2" name="selItem">
								<?php
								foreach ($allItems as $key) {
								echo '<option value="' . $key->ItemId . '">' . $key->ItemName . '</option>';
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Select College</label>
							<select data-placeholder="Choose a College..." class="chosen-select" id="selectCollege" name="selectCollege"  tabindex="2" name="selItem">
								<?php
								foreach ($allCollege as $key_c) {
								echo '<option value="' . $key_c->CollegeId . '">' . $key_c->CollegeName . '</option>';
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Select Result Type</label>
							<select data-placeholder="Choose a result type..." class="chosen-select" id="SelectType" name="selectType"  tabindex="2" name="selItem" >
								<option value="F">First</option>
								<option value="S">Second</option>
								<option value="T">Third</option>
								<option value="X">A Grade</option>
							</select>
						</div>
						<div class="form-group">
							<label>Register No</label>
							<input type="text" name="txtAdmissionNo" class="form-control" value="" placeholder="Enter register no">
						</div>
						<div class="form-group">
							<button type="submit" value="save" class="btn btn-success pull-right ">Save</button>
							<button type="reset" value="save" class="btn btn-danger ">Reset</button>
						</div>
						</form>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>

</div>
</div>