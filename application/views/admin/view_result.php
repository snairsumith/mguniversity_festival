<div class="col-md-12">

	<div class="ibox">


        
		<div class="ibox-title">
			

			<h5>Item List</h5>

		</div>
		<div class="ibox-content">
			<table class="table">
				<thead>
					<tr>
						
						<th>Item Name</th>
						<th>Student Name</th>
						<th>College Name</th>
						<th>Admission No</th>
						<th>Point</th>
						<th>Result Type</th>
						
					</tr>
				</thead>
				<tbody>
					<?php foreach ($allResult as $key ) { ?>
						<tr>
							<td><?php echo $key->ItemName ?></td>
							<td><?php echo $key->Name ?></td>
							<td><?php echo $key->CollegeName ?></td>
							<td><?php echo $key->AdmissionNo ?></td>
							<td><?php echo $key->Point ?></td>
							<td><?php echo $key->ResultType ?></td>
						</tr>
							
					<?php	} ?>
					
				</tbody>
			</table>
			
		</div>
	</div>
</div>