<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"> -->


	<style type="text/css">
b, strong {
    font-weight: bolder;
}

.text-center {
    text-align: center!important;
}
@media (min-width: 768px)
.ml-md-1, .mx-md-1 {
    margin-left: .25rem!important;
}
	.text-right {
    text-align: right!important;
}
@media (min-width: 768px)
.ml-md-5, .mx-md-5 {
    margin-left: 3rem!important;
}






	@media (min-width: 1200px)
.container {
    max-width: 1140px;
}

@media (min-width: 992px)
.container {
    max-width: 960px;
}
@media (min-width: 768px)
.container {
    max-width: 720px;
}
@media (min-width: 576px)
.container {
    max-width: 540px;
}
.container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}
	.table {
    width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
}



.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
@media (min-width: 768px)
.col-md-12 {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
}

.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    position: relative;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
}
th{
	font-size:12px;
}
td{
	font-size:10px;
}


body {
    margin: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center">MAHATMA GANDHI UNIVERSITY UNION</h1>
			<h3 class="text-center">YOUTH FESTIVAL – ALATHAALAM 2019</h3>
			<br/>
			<h4 class="text-center"><strong>REGISTRATION FORM</strong></h4>
			<br/>
			<div class="row">
			<!-- <div class="col-md-4">
				<h5  class="ml-md-5 text-right">COLLEGE NAME : <?php echo $CollegeDeatils[0]->CollegeName ?></h5>
				<br/>
				<br/>
			</div> -->

			<div class="col-md-6">
				
			</div>
			</div>
			<!-- <div class="row">
				<div class="col-md-4">
					<br/>
					<h4 class="mr-md-5 text-right">REGISTRATION DETAILS :</h4>
					<h5 class="mr-md-5 text-right">TOTAL NUMBER OF INDIVIDUAL ITEMS : <?php echo count($TotalSingleItem); ?> </h5>
					<h5 class="mr-md-5 text-right">TOTAL NUMBER OF GROUP ITEMS :  <?php echo count($GroupItemCount); ?></h5>
					<h5 class="mr-md-5 text-right">TOTAL NUMBER OF PARTICIPANTS : <?php echo $TotalParticipant; ?></h5>
					<br/><br/><br/><br/>
				</div>
			</div> -->
			<div class="row">
				<div class="col-md-12">
					<br/><br/><br/>
				<h3 class="text-center">DECLARATION</h3>
				<br/><br/><br/><br/>
				<h4>I, here by declare that all the details mentioned in this form are
correct to the best of my knowledge and the participants
mentioned are the students of this college.</h4>
			</div>
			</div>
			<div class="row">
				<table>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><h4>Place :</h4></td>
						<td><h4>Date :</h4></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><h4>Signature of head of the institution/</h4></td>
						<td><h4>college with official seal</h4></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
				
			</div>