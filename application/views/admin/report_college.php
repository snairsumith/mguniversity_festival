<style type="text/css">
	.test-width{
		width:1020px !important;
	}
	.table{
		width:70% !important;
	}
</style>
<div class="row">
	<div class="col-md-12">
		
	</div>
</div>
<div class="col-md-12">
	<div class="ibox">
		<a href ="<?php echo site_url() ?>Admin/generatePDFFile" class="btn btn-primary pull-right mb-2"><i class="fa fa-list"></i>&nbsp;Genarate Report</a>
		<div class="ibox-title">
			<h5>College Wise Item Deatils</h5>
		</div>
		<div class="ibox-content">
			<?php $collegeCount=1;
			foreach ($allRegisterdCollege as $key) { ?>
					<br/>
			<h1><?php echo $collegeCount.')'.$key->CollegeName; ?></h1>
			<br/>
			<h4 class="text-danger">Single Item</h4>
			<table class="table table-bordered">
				<thead>
					<th>Item Name </th>
					<th>Student Name </th>
					<th>Student Mobile</th>
					<th>Student Email</th>
				</thead>
				<tbody>
					<?php foreach ($allParticipant as $key1_single) {
						if(($key->CollegeId===$key1_single->CollegeId)&&($key1_single->ItemTypeId==2)){
					?>
					
					
					<tr>
						<td><?php echo $key1_single->ItemName; ?></td>
						<td><?php echo $key1_single->Name;?></td>
						<td><?php echo $key1_single->PhoneNo;?></td>
						<td><?php echo $key1_single->EmailId;?></td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
			<h4 class="text-danger">Group Item</h4>
			<table class="table table-bordered">
				<thead>
					<th>Item Name </th>
					<th>Student Name </th>
					<th>Student Mobile</th>
					<th>Student Email</th>
				</thead>
				<tbody>
					<?php foreach ($allParticipant as $key1_single) {
						if(($key->CollegeId===$key1_single->CollegeId)&&($key1_single->ItemTypeId==1)){
					?>
					
					
					<tr>
						<td><?php echo $key1_single->ItemName; ?></td>
						<td><?php echo $key1_single->Name;?></td>
						<td><?php echo $key1_single->PhoneNo;?></td>
						<td><?php echo $key1_single->EmailId;?></td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
			
			<?php
			$collegeCount=$collegeCount+1;
			}
			?>
		</div>
	</div>
</div>
