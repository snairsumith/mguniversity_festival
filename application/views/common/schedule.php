<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MG UNIVERSITY  Kalolsavam 2019</title>
        
        <!-- Styles -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css"><!-- Google web fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
        <link href="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
        <link href="<?php echo site_url() ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
        <link href="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
        <link href="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
        <link href="<?php echo site_url() ?>/assets/style.css" rel="stylesheet" type="text/css"><!-- theme styles -->
        <link href="<?php echo site_url() ?>img/fav.ico" rel="shortcut icon" type="image/x-icon" />
    </head>
    
    <body role="document">
        
        <!-- device test, don't remove. javascript needed! -->
        <span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
        <!-- device test end -->
        
        <div id="k-head" class="container"><!-- container + head wrapper -->
        
        <div class="row"><!-- row -->
        
        
        
        <div class="col-lg-12">
            
            <div id="k-site-logo" class="pull-left"><!-- site logo -->
            
            <h1 class="k-logo">
            <a href="index.html" title="Home Page">
                <img src="<?php echo site_url() ?>/img/site-logo.png" alt="Site Logo" class="img-responsive" />
            </a>
            </h1>
            
            <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->
            
            </div><!-- site logo end -->
            <nav id="k-menu" class="k-main-navig"><!-- main navig -->
            
            <ul id="drop-down-left" class="k-dropdown-menu">
                <li>
                    <a href="<?php echo site_url() ?>" title="Home ">Home</a>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/schedule" title="Kalolsavam Schedule">Schedule</a>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/rules" title="Kalolsavam Rules">Rules</a>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/instruction" title="Genaral Instruction">Instructions</a>
                </li>
                <li>
                    <a href="#" title="Kalolsavam Result">Result</a>
                    <ul class="sub-menu">
                        <li><a href="<?php echo site_url() ?>common/result">College Wise</a></li>
                        <li><a href="<?php echo site_url() ?>common/item_result">Item Wise</a></li>
                        <li><a href="<?php echo site_url() ?>common/individual_result">Individual Result</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/login" title="Register Candidates">Login</a>
                </li>
                
                
            </ul>
            
            </nav><!-- main navig end -->
            
        </div>
        
        </div><!-- row end -->
        
        </div><!-- container + head wrapper end -->
        
        <div id="k-body"><!-- content wrapper -->
        
        <div class="container"><!-- container -->
        
        <div class="row"><!-- row -->
        <div class="col-md-12">
            <!--  <img src="<?php echo site_url() ?>/doc/schedule.jpg"> -->
            <table class="table table-striped table-courses">
                <thead>
                    <tr>
                        
                        <th>Day</th>
                        <th>Stage</th>
                        <th>Item</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($allSchedule as $key ) { ?>
                        <tr>
                            <td><?php 

                            if($key->Day==1){
                                echo "Feb 28 ,Thursday";
                            }else if($key->Day==2){
                                echo "March 1 ,Friday";
                            }else if($key->Day==3){
                                echo "March 2,Saturday";
                            }else if($key->Day==4){
                                echo "March 3 ,Sunday";
                            }else if($key->Day==5){
                                echo "March 4 ,Monday";
                            }
                            ?></td>
                            <td>
                                <?php 

                                if($key->Stage==1){
                                echo "Stage 1-Abimaniyu Nagar";
                            }else if($key->Stage==2){
                                echo "Stage 2-Simon Britto Nagar";
                            }else if($key->Stage==3){
                                echo "Stage 3-Lenin Rajendran Nagar";
                            }else if($key->Stage==4){
                                echo "Stage 4-Balabhaskar Nagar";
                            }else if($key->Stage==5){
                                echo "Stage 5-Mrinal Sen Nagar";
                            }else if($key->Stage==6){
                                echo "Stage 6-Sister Lini Nagar";
                            }else if($key->Stage==7){
                                echo "Stage 7-Captain Raju Nagar";
                            }

                                 ?>
                            
                         </td>
                            
                            <td><?php echo $key->ItemName ?></td>
                            <td><?php echo $key->Time ?></td>
                            
                    <?php   } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<div id="k-footer"></div><!-- footer end -->

<div id="k-subfooter"><!-- subfooter -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->

<div class="col-lg-12">

<p class="copy-text text-inverse">
    &copy; Design & Developed by <a href="http://minusbugs.com">minusbugs.com</a>
</p>

</div>

</div><!-- row end -->

</div><!-- container end -->

</div><!-- subfooter end -->
<!-- jQuery -->
<script src="<?php echo site_url() ?>/assets/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo site_url() ?>/assets/jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="<?php echo site_url() ?>/assets/bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="<?php echo site_url() ?>/assets/js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="<?php echo site_url() ?>/assets/js/jquery.easy-pie-chart.js"></script>



<!-- Theme -->
<script src="<?php echo site_url() ?>/assets/js/theme.js"></script>
</body>
</html>