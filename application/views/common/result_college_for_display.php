<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <!--  <title>MG UNIVERSITY  Kalolsavam 2019</title> -->
    
    <!-- Styles -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="<?php echo site_url() ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
 <link href="<?php echo site_url() ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="<?php echo site_url() ?>/assets/style.css" rel="stylesheet" type="text/css"><!-- theme styles -->
    <style type="text/css">
        .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 15%;
}
h6,h4{
    margin-top:5px !important;
}
.table td{
    margin:3px 8px !important;
}
.table>tbody>tr>td{
    padding:3px !important;
}
.table .thead-light th {
    color: #495057;
    background-color: #e9ecef;
    border-color: #dee2e6;
}
.color-white{
    color:#fff;
}
body {
        --color-text: #f4f4f4;
    --color-bg: #ff1e21;
    --color-bg-2: #f98e4a;
    --color-link: #317bd0;
    --color-link-hover: #317bd0;
    background: radial-gradient(ellipse at 100% 0, #af0307 0, var(--color-bg) 50%, #3a0101 115%);
}
    </style>
  </head>
<body>
    <div class="container"> 
        <div class="overlay"></div>
        <main>
            <div class="frame">
                
                
            </div>
        </main>
        <div class="row no-gutter">
            <div class="col-md-12">
              <img src="<?php echo site_url() ?>img/logo-for-result.png" class="center"> 
                <h1 class="text-center page-title color-white">MG UNIVERSITY  KALOLSAVAM 2019 -COLLEGE WISE RESULT</h1>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead class="thead-light">
                        <th class="text-center"><h4>SL No</h4></th>
                        <th class="text-center"><h4>College Name</h4></th>
                        <th class="text-center"><h4>Point</h4></th>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach ($topCollege as $key) { ?>
                            
                        <tr>
                            <td class="text-center"><h6 class="color-white"><?php echo $i ?></h6></td>
                            <td class="text-center"><h6  class="color-white"><?php echo $key->CollegeName; ?></h6></td>
                            <td class="text-center"><h4  class="color-white"><?php echo $key->TotalPoint; ?></h4></td>
                        </tr>
                    <?php
                    $i=$i+1;
                     } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</body>

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>/assets/jQuery/jquery-2.1.1.min.js"></script>
    <script src="<?php echo site_url() ?>/assets/jQuery/jquery-migrate-1.2.1.min.js"></script>
    
    <!-- Bootstrap -->
    <script src="<?php echo site_url() ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    


    
    

</html>