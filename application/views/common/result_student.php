<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MG UNIVERSITY  Kalolsavam 2019</title>
        
        <!-- Styles -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css"><!-- Google web fonts -->
        <link href="<?php echo site_url() ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
        <link href="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
        <link href="<?php echo site_url() ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
        <link href="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
        <link href="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
        <link href="<?php echo site_url() ?>/assets/style.css" rel="stylesheet" type="text/css"><!-- theme styles -->
        <link href="<?php echo site_url() ?>img/fav.ico" rel="shortcut icon" type="image/x-icon" />
        <link href="<?php echo site_url() ?>css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    </head>
    
    <body role="document">
        
        <!-- device test, don't remove. javascript needed! -->
        <span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
        <!-- device test end -->
        
        <div id="k-head" class="container"><!-- container + head wrapper -->
        
        <div class="row"><!-- row -->
        
        
        
        <div class="col-lg-12">
            
            <div id="k-site-logo" class="pull-left"><!-- site logo -->
            
            <h1 class="k-logo">
            <a href="index.html" title="Home Page">
                <img src="<?php echo site_url() ?>/img/site-logo.png" alt="Site Logo" class="img-responsive" />
            </a>
            </h1>
            
            <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->
            
            </div><!-- site logo end -->
            <nav id="k-menu" class="k-main-navig"><!-- main navig -->
            
            <ul id="drop-down-left" class="k-dropdown-menu">
                <li>
                    <a href="<?php echo site_url() ?>" title="Home ">Home</a>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/schedule" title="Kalolsavam Schedule">Schedule</a>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/rules" title="Kalolsavam Rules">Rules</a>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/instruction" title="Genaral Instruction">Instructions</a>
                </li>
                
                <li>
                    <a href="#" title="Kalolsavam Result">Result</a>
                    <ul class="sub-menu">
                        <li><a href="<?php echo site_url() ?>common/result">College Wise</a></li>
                        <li><a href="<?php echo site_url() ?>common/item_result">Item Wise</a></li>
                        <li><a href="<?php echo site_url() ?>common/individual_result">Individual Result</a></li>
                        <li><a href="<?php echo site_url() ?>common/result_by_student">Result By Student</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo site_url() ?>common/login" title="Register Candidates">Login</a>
                </li>
                
                
            </ul>
            
            </nav><!-- main navig end -->
            
        </div>
        
        </div><!-- row end -->
        
        </div><!-- container + head wrapper end -->
        
        <div id="k-body"><!-- content wrapper -->
        
        <div class="container"><!-- container -->
        
        <div class="row"><!-- row -->
        <div class="col-padded">
            <div class="row gutter">
                <div class="col-md-12">
                    <h1 class="page-title">Result By Student</h1>
                </div>
                <div class="row gutter" style="height:450px;">
                    <div class="col-lg-12 col-md-12">
                       
                        <div class="row">
                            <?php echo form_open('Common/result_by_student_action'); ?>
                            <div class="col-md-8">
                                <input type="text" name="txtStudentAdmissionNo" class="form-control" placeholder="Enter Student Admission No">
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-success">Search</button>
                            </div>
                        </form>
                        </div>

                        <div class="row gutter">
                            
                            <div class="col-lg-12 col-md-12">
                                <div id="loader" style="display:none;">
                                    <img src="<?php echo site_url() ?>/img/loader.gif" width="100" height="100"/>
                                </div>
                                <?php if(count($result)==0){
                                    echo "<h1>No Record Found</h1>";
                                } ?>
                                <table class="table table-striped table-courses">
                                    <thead>
                                        <th ><strong>Item Name</strong></th>
                                        <th ><strong>Result </strong></th>
                                        <th ><strong>Point</strong></th>
                                        
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $key ) { ?>
                                            <tr>
                                            <td><?php echo $key->ItemName ?></td>
                                            <td><?php if($key->ResultType=="F"){
                                                   echo "First";
                                                   }
                                                   else if($key->ResultType=="S"){
                                                    echo "Second";
                                                   }else if($key->ResultType=="T"){
                                                    echo "Third";
                                                   }else if($key->ResultType=="X"){
                                                    echo "A Grade";
                                                   }?></td>
                                            <td><?php echo $key->Point ?></td>
                                        </tr>

                                        <?php }?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
</div>
</div>
<div id="k-footer"></div><!-- footer end -->

<div id="k-subfooter"><!-- subfooter -->

<div class="container"><!-- container -->

<div class="row"><!-- row -->

<div class="col-lg-12">

<p class="copy-text text-inverse">
    &copy; Design & Developed by <a href="http://minusbugs.com">minusbugs.com</a>
</p>

</div>

</div><!-- row end -->

</div><!-- container end -->

</div><!-- subfooter end -->
<!-- jQuery -->
<script src="<?php echo site_url() ?>/assets/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo site_url() ?>/assets/jQuery/jquery-migrate-1.2.1.min.js"></script>

<!-- Bootstrap -->
<script src="<?php echo site_url() ?>/assets/bootstrap/js/bootstrap.min.js"></script>

<!-- Drop-down -->
<script src="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.js"></script>

<!-- Fancybox -->
<script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->

<!-- Responsive videos -->
<script src="<?php echo site_url() ?>/assets/js/jquery.fitvids.js"></script>

<!-- Audio player -->
<script src="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.min.js"></script>

<!-- Pie charts -->
<script src="<?php echo site_url() ?>/assets/js/jquery.easy-pie-chart.js"></script>



<!-- Theme -->
<script src="<?php echo site_url() ?>/assets/js/theme.js"></script>
<!-- Chosen -->
<script src="<?php echo site_url() ?>js/plugins/chosen/chosen.jquery.js"></script>

<script type="text/javascript">
$('#selectItem').chosen({width: "100%"});
function get_itemwiseResult(id){
$('#loader').css('display','block');
var baseurl="http://mgukalolsavam19.com/";
var html="";
// var baseurl="http://localhost/mguniversity_festival/";
var url=baseurl+'Festival_api/get_itemwise_result/'+id;
$.get(url,function(data){

if(data.resultCode==1){
$.each(data.data, function( index, value ) {
resultName="";
if(value.ResultType=='F'){
resultName="First";
}else if(value.ResultType=="S"){
resultName="SECOND";
}else if(value.ResultType=="T"){
resultName="THIRD";
}else if(value.ResultType=="X"){
resultName="A Grade";
}
if(value.ItemType==1){
html=html+"<tr><td><strong>"+resultName+"</strong></td><td><strong>"+value.CollegeName+"</strong></td></tr>";
$('#th_studentname').css('display','none');
}else if(value.ItemType==2){
html=html+"<tr><td><strong>"+resultName+"</strong></td><td><strong>"+value.CollegeName+"</strong></td><td><strong>"+value.Name+"</strong></td></tr>";
$('#th_studentname').css('display','block');
}
});
$('#result_item').html(html);
$('#loader').css('display','none');
}else{
html=html+"<tr><td><h2>No Result Found</h2></td></tr>";
$('#result_item').html(html);
$('#loader').css('display','none');
}
});
}
</script>
</body>
</html>