<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>MG UNIVERSITY-Login Page</title>

    <link href="<?php echo site_url() ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo site_url() ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">MAHATMA GANDHI UNIVERSITY </h2>

                <p>
                    Kalolsavam 2019 
                </p>

                <p>
                    Manage MGU Kalolsavam 2019 Item ,College & Participants Deatils Here
                </p>

               

               
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <div class="m-t">
                        <?php echo form_open('Common/login_action'); ?>
                        <div class="form-group">
                            <input type="email" name="txtEmail" class="form-control" placeholder="Username" >
                        </div>
                        <div class="form-group">
                            <input type="password" name="txtPassword" class="form-control" placeholder="Password" >
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                        <p class="text-danger text-center">
                             <?php if($isLogin==2){echo "Incorrect username or password";} elseif($isLogin==3){echo "Registration Over.Contact administrator";}?>
                        </p>
                    </form>
                    <a href="<?php echo site_url() ?>">Go To Home</a>
                    </div>
                   
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                MG UNIVERSITY
            </div>
            <div class="col-md-6 text-right">
               <small>© 2019</small>
            </div>
        </div>
    </div>

</body>
    <script src="<?php echo site_url() ?>js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo site_url() ?>js/mgu.js"></script>
</html>
