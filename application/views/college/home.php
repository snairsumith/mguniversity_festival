<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MG UNIVERSITY  Kalolsavam 2019</title>
    
    <!-- Styles -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="<?php echo site_url() ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="<?php echo site_url() ?>/assets/style.css" rel="stylesheet" type="text/css"><!-- theme styles -->
  <link href="<?php echo site_url() ?>img/fav.ico" rel="shortcut icon" type="image/x-icon" />
  </head>
  
  <body role="document">
  
    <!-- device test, don't remove. javascript needed! -->
    <span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
    <!-- device test end -->
    
    <div id="k-head" class="container"><!-- container + head wrapper -->
    
        <div class="row"><!-- row -->
        
          
        
            <div class="col-lg-12">
        
                <div id="k-site-logo" class="pull-left"><!-- site logo -->
                
                    <h1 class="k-logo">
                        <a href="<?php echo site_url() ?>" title="Home Page">
                            <img src="<?php echo site_url() ?>/img/site-logo.png" alt="Site Logo" class="img-responsive" />
                        </a>
                    </h1>
                    
                    <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->
            
                </div><!-- site logo end -->

                <nav id="k-menu" class="k-main-navig"><!-- main navig -->
        
                    <ul id="drop-down-left" class="k-dropdown-menu">
                        <li>
                            <a href="<?php echo site_url() ?>" title="Home ">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>common/schedule" title="Kalolsavam Schedule">Schedule</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>common/rules" title="Kalolsavam Rules">Rules</a>
                        </li>
                         <li>
                            <a href="<?php echo site_url() ?>common/instruction" title="Genaral Instruction">Instructions</a>
                        </li>
                          <li>
                            <a href="<?php echo site_url() ?>common/result" title="Kalolsavam Result">Result</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>common/login" title="Register Candidates">Login</a>
                        </li>
                        
                        
                    </ul>
        
                </nav><!-- main navig end -->
            
            </div>
            
        </div><!-- row end -->
    
    </div><!-- container + head wrapper end -->
    
    <div id="k-body"><!-- content wrapper -->
    
        <div class="container"><!-- container -->
        
            <div class="row"><!-- row -->
            
                <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->
                
                    <form action="#" id="top-searchform" method="get" role="search">
                        <div class="input-group">
                            <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
                        </div>
                    </form>
                    
                    <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->
                
                </div><!-- top search end -->
            
            
                
            </div><!-- row end -->
            
            <div class="row no-gutter fullwidth"><!-- row -->
            
                <div class="col-lg-12 clearfix"><!-- featured posts slider -->
                
                    <div id="carousel-featured" class="carousel slide" data-interval="4000" data-ride="carousel"><!-- featured posts slider wrapper; auto-slide -->
                    
                        <ol class="carousel-indicators"><!-- Indicators -->
                            <li data-target="#carousel-featured" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-featured" data-slide-to="1"></li>
                            <li data-target="#carousel-featured" data-slide-to="2"></li>
                            <li data-target="#carousel-featured" data-slide-to="3"></li>
                            <li data-target="#carousel-featured" data-slide-to="4"></li>
                        </ol><!-- Indicators end -->
                    
                        <div class="carousel-inner"><!-- Wrapper for slides -->
                        
                            <div class="item active">
                                <img src="img/slide-3.jpg" alt="Image slide 3" />
                              
                            </div>
                            
                            <div class="item">
                                <img src="img/slide-2.jpg" alt="Image slide 2" />
                                
                            </div>
                            
                            <div class="item">
                                <img src="img/slide-1.jpg" alt="Image slide 1" />
                               
                            </div>
                            
                            <div class="item">
                                <img src="img/slide-4.jpg" alt="Image slide 4" />
                                
                            </div>
                            
                            <div class="item">
                                <img src="img/slide-5.jpg" alt="Image slide 5" />
                                
                            </div>
                            
                        </div><!-- Wrapper for slides end -->
                    
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-featured" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="right carousel-control" href="#carousel-featured" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                        <!-- Controls end -->
                        
                    </div><!-- featured posts slider wrapper end -->
                        
                </div><!-- featured posts slider end -->
                
            </div><!-- row end -->
            
            <div class="row no-gutter"><!-- row -->
                
               
                
                <div class="col-lg-8 col-md-8"><!-- recent news wrapper -->
                    
                    <div class="col-padded"><!-- inner custom column -->
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_recent_news"><!-- widgets list -->
                    
                                <h1 class="title-widget">About MGU Kalolsavam 2019</h1>
                                
                                <ul class="list-unstyled">
                                
                                    <li class="recent-news-wrap">
                                
                                        <h1 class="title-median"><a href="#" title="Megan Boyle flourishes...">Alathaalam</a></h1>
                                        
                                        <div class="recent-news-meta">
                                            <div class="recent-news-date">2019-2020</div>
                                        </div>
                                        
                                        <div class="recent-news-content clearfix">
                                            <figure class="recent-news-thumb">
                                                <a href="#" title="Megan Boyle flourishes..."><img src="img/recent-news-thumb-1.jpg" class="attachment-thumbnail wp-post-image" alt="Thumbnail 1" /></a>
                                            </figure>
                                            <div class="recent-news-text">
                                                <p>
                                                The Inter Collegiate Youth Festivals are organized by the University Union annually to promote art and cultural talent among the students.  It also aims to encourage friendship and fellowship among young artists by giving them an opportunity to display their talents on a common platform. The date and venue of the festival is fixed by the Union in consultation with the Vice-Chancellor. There is a Steering cum Appellate Committee (SAC) to supervise and control the conduct of the programme. <a href="https://www.mgu.ac.in/about/authorities/offices-of-the-university/department-of-student-services/youth-festivals/" class="moretag" title="read more">MORE</a> 
                                                </p>
                                            </div>
                                        </div>
                                    
                                    </li>
                                    
                                    
                                
                                </ul>
                                
                            </li><!-- widgets list end -->
                        
                        </ul><!-- widgets end -->
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- recent news wrapper end -->
                
                <div class="col-lg-4 col-md-4"><!-- misc wrapper -->
                    
                    <div class="col-padded col-shaded" style="padding-top:2px !important"><!-- inner custom column -->
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_course_search"></li><!-- widget end -->
                            
                            <li class="widget-container widget_text"><!-- widget -->
                            
                                <a href="<?php echo site_url() ?>common/rules" class="custom-button cb-green" title="How to apply?">
                                    <i class="custom-button-icon fa fa-check-square-o"></i>
                                    <span class="custom-button-wrap">
                                        <span class="custom-button-title">Kalolsavam Rules 2019</span>
                                        <span class="custom-button-tagline">Check the rules</span>
                                    </span>
                                    <em></em>
                                </a>
                                
                                <a href="<?php echo site_url() ?>common/schedule" class="custom-button cb-gray" title="Campus tour">
                                    <i class="custom-button-icon fa  fa-play-circle-o"></i>
                                    <span class="custom-button-wrap">
                                        <span class="custom-button-title">Kalolsavam Schedule</span>
                                        <span class="custom-button-tagline">Check the kalolsavam schedule</span>
                                    </span>
                                    <em></em>
                                </a>
                                
                                <a href="<?php echo site_url() ?>common/instruction" class="custom-button cb-yellow" title="Prospectus">
                                    <i class="custom-button-icon fa  fa-leaf"></i>
                                    <span class="custom-button-wrap">
                                        <span class="custom-button-title">General Instructions</span>
                                        <span class="custom-button-tagline">kalolsavam general instruction</span>
                                    </span>
                                    <em></em>
                                </a>
                            
                            </li><!-- widget end -->
                            
                                
                        </ul><!-- widgets end -->
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- misc wrapper end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- content wrapper end -->
    
    <div id="k-footer"></div><!-- footer end -->
    
    <div id="k-subfooter"><!-- subfooter -->
    
        <div class="container"><!-- container -->
        
            <div class="row"><!-- row -->
            
                <div class="col-lg-12">
                
                    <p class="copy-text text-inverse">
                    &copy; Design & Developed by <a href="http://minusbugs.com">minusbugs.com</a>
                    </p>
                
                </div>
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- subfooter end -->

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>/assets/jQuery/jquery-2.1.1.min.js"></script>
    <script src="<?php echo site_url() ?>/assets/jQuery/jquery-migrate-1.2.1.min.js"></script>
    
    <!-- Bootstrap -->
    <script src="<?php echo site_url() ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Drop-down -->
    <script src="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.js"></script>
    
    <!-- Fancybox -->
    <script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.pack.js"></script>
    <script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->
    
    <!-- Responsive videos -->
    <script src="<?php echo site_url() ?>/assets/js/jquery.fitvids.js"></script>
    
    <!-- Audio player -->
    <script src="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.min.js"></script>
    
    <!-- Pie charts -->
    <script src="<?php echo site_url() ?>/assets/js/jquery.easy-pie-chart.js"></script>
    
    
    
    <!-- Theme -->
    <script src="<?php echo site_url() ?>/assets/js/theme.js"></script>
    
  </body>
</html>