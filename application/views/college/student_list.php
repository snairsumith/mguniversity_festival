<div class="col-md-12">
	<div class="ibox">
		<?php if($registerType=="group") {?>
		<a href ="<?php echo site_url() ?>College/students_register_group/group" class="btn btn-primary pull-right mb-2"><i class="fa fa-plus"></i>&nbsp;Register New</a>
		<?php }else if($registerType=="single") {?>
		<a href ="<?php echo site_url() ?>College/students_register_single/single" class="btn btn-primary pull-right mb-2"><i class="fa fa-plus"></i>&nbsp;Register New</a>
		<?php } ?>
		<div class="ibox-title">
			<h5>Student List</h5>
		</div>
		<div class="ibox-content">
				<?php if($registerType=="group") {?>
		<table class="table">
				<thead>
					<tr>
						<th>Item Name</th>
						<th>Item Desc</th>
						
						<th>Action</th>
					</tr>
				</thead>
				<tbody >
					<?php foreach ($allRegisterdGroupItem as $key1 ) { ?>
					<tr>
					<td><?php echo $key1->ItemName ?></td>
					<td><?php echo $key1->ItemDescription ?></td>
					<td>
						<button class="btn-info btn btn-xs" data-toggle="modal" data-target="#myModelStudentDeatils" onclick="fetchStudentDetailsByGroup('<?php echo $key1->ItemId ?>')">View Registerd Student</button>
					</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>



			<div class="modal inmodal" id="myModelStudentDeatils" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content animated fadeIn">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			
			<h4 class="modal-title">Participant Details</h4>
			<small>View all registred  participant details</small>
		</div>
		<div class="modal-body">
			<table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Register No</th>
                                    <th>Name</th>
                                    <th>Phone No</th>
									<td>Class</td>                                    
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="tbody_group_student_item">
                                <!-- <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td><a href="" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Delete </a></td>
                                </tr> -->
                                
                                </tbody>
                            </table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			
		</div>
	</div>
</div>
</div>

				<?php }else{ ?>
			<table class="table">
				<thead>
					<tr>
						<th>Register No</th>
						<th>Name Of Student</th>
						<th>Course</th>
						<th>Class</th>
						<th>Mobile No</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody >
				<tr id="tbody_college"></tr>
				<?php foreach ($allStudents as $key ) { ?>
				<tr>
					<td><?php echo $key->AdmissionNo ?></td>
					<td><?php echo $key->Name ?></td>
					<td><?php echo $key->Course ?></td>
					<td><?php echo $key->Class ?></td>
					<td><?php echo $key->PhoneNo ?></td>
					<td>
						<button class="btn-info btn btn-xs" data-toggle="modal" data-target="#myModelItemDeatils" onclick="fetchStudentsItems('<?php echo $key->AdmissionNo ?>')">View Items</button>
					</td>
					
					<?php	} ?>
				</tr>
			</tbody>
		</table>

<div class="modal inmodal" id="myModelItemDeatils" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content animated fadeIn">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			
			<h4 class="modal-title">Single Item Details</h4>
			<small>View all registred  item details</small>
		</div>
		<div class="modal-body">
			<table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Register Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="tbody_student_item">
                                <!-- <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td><a href="" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Delete </a></td>
                                </tr> -->
                                
                                </tbody>
                            </table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			
		</div>
	</div>
</div>
</div>


	<?php } ?>
		<input type="hidden" name="hdCollegeId" id="hdCollegeId" value="<?php echo $collegeId; ?>">
	</div>
</div>
</div>

