<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MG UNIVERSITY  Kalolsavam 2019</title>
    
    <!-- Styles -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700,800" rel="stylesheet" type="text/css"><!-- Google web fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- font-awesome -->
    <link href="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css"><!-- dropdown-menu -->
    <link href="<?php echo site_url() ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- Bootstrap -->
    <link href="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"><!-- Fancybox -->
    <link href="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.css" rel="stylesheet" type="text/css"><!-- Audioplayer -->
    <link href="<?php echo site_url() ?>/assets/style.css" rel="stylesheet" type="text/css"><!-- theme styles -->
     <link href="<?php echo site_url() ?>img/fav.ico" rel="shortcut icon" type="image/x-icon" />
  </head>
  
  <body role="document">
  
    <!-- device test, don't remove. javascript needed! -->
    <span class="visible-xs"></span><span class="visible-sm"></span><span class="visible-md"></span><span class="visible-lg"></span>
    <!-- device test end -->
    
    <div id="k-head" class="container"><!-- container + head wrapper -->
    
        <div class="row"><!-- row -->
        
          
        
            <div class="col-lg-12">
        
                <div id="k-site-logo" class="pull-left"><!-- site logo -->
                
                    <h1 class="k-logo">
                        <a href="index.html" title="Home Page">
                           <img src="<?php echo site_url() ?>/img/site-logo.png" alt="Site Logo" class="img-responsive" />
                        </a>
                    </h1>
                    
                    <a id="mobile-nav-switch" href="#drop-down-left"><span class="alter-menu-icon"></span></a><!-- alternative menu button -->
            
                </div><!-- site logo end -->

                <nav id="k-menu" class="k-main-navig"><!-- main navig -->
        
                    <ul id="drop-down-left" class="k-dropdown-menu">
                        <li>
                            <a href="<?php echo site_url() ?>" title="Home ">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>common/schedule" title="Kalolsavam Schedule">Schedule</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>common/rules" title="Kalolsavam Rules">Rules</a>
                        </li>
                         <li>
                            <a href="<?php echo site_url() ?>common/instruction" title="Genaral Instruction">Instructions</a>
                        </li>
                          <li>
                            <a href="<?php echo site_url() ?>common/result" title="Kalolsavam Result">Result</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>common/login" title="Register Candidates">Login</a>
                        </li>
                        
                        
                    </ul>
        
                </nav><!-- main navig end -->
            
            </div>
            
        </div><!-- row end -->
    
    </div><!-- container + head wrapper end -->
    
    <div id="k-body"><!-- content wrapper -->
    
        <div class="container"><!-- container -->
        
            <div class="row"><!-- row -->
                <div class="col-lg-12 col-md-12">
                    <h1>GENERAL INSTRUCTIONS</h1>
                    <ul>
                        <li>READ ALL THE RULES SPECIFIED PRIOR TO REGISTRATION.</li>
                        <li>PRINTOUT OF THE REGISTRATION FORM SHOULD BE
SUBMITTED IN THE UNION OFFICE ON OR BEFORE THE
LAST DATE OF REGISTRATION.THE SAME SHOULD BE
SIGNED AND SEALED BY THE COLLEGE PRINCIPAL.</li>
                        <li>PARTICIPANTS MUST HAVE THEIR COLLEGE ID CARD WITH
THEM.</li>
                       
                       <div class="col-lg-6 col-md-6 col-sm-12">
                                    <h6>IF YOU HAVE ANY QUERY, CONTACT</h6>
                                    <blockquote>
                                        <h4>CHAIRMAN</h4><p>NIKHIL S NAIR 8606501203</p>
                                         <h4>GENERAL CONVENOR</h4><p>ARUN K M 8606170434</p>
                                       
                                    </blockquote>
                                </div>
                </div>
            </div>
        </div>
    </div>
     <div id="k-footer"></div><!-- footer end -->
    
    <div id="k-subfooter"><!-- subfooter -->
    
        <div class="container"><!-- container -->
        
            <div class="row"><!-- row -->
            
                <div class="col-lg-12">
                
                    <p class="copy-text text-inverse">
                    &copy; Design & Developed by <a href="http://minusbugs.com">minusbugs.com</a>
                    </p>
                
                </div>
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- subfooter end -->

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>/assets/jQuery/jquery-2.1.1.min.js"></script>
    <script src="<?php echo site_url() ?>/assets/jQuery/jquery-migrate-1.2.1.min.js"></script>
    
    <!-- Bootstrap -->
    <script src="<?php echo site_url() ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Drop-down -->
    <script src="<?php echo site_url() ?>/assets/js/dropdown-menu/dropdown-menu.js"></script>
    
    <!-- Fancybox -->
    <script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox.pack.js"></script>
    <script src="<?php echo site_url() ?>/assets/js/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->
    
    <!-- Responsive videos -->
    <script src="<?php echo site_url() ?>/assets/js/jquery.fitvids.js"></script>
    
    <!-- Audio player -->
    <script src="<?php echo site_url() ?>/assets/js/audioplayer/audioplayer.min.js"></script>
    
    <!-- Pie charts -->
    <script src="<?php echo site_url() ?>/assets/js/jquery.easy-pie-chart.js"></script>
    
    
    
    <!-- Theme -->
    <script src="<?php echo site_url() ?>/assets/js/theme.js"></script>
</body>
</html>