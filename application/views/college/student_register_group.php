<div class="col-md-12">
	<div class="ibox">
		<div class="ibox-title">
			<div class="row">
				<div class="col-md-8">
					<h5>Register Student</h5>
				</div>
				<div class="col-md-4">
					<div class="row">
						<h5 class="text-danger" id="hParticipant">Total Allowed Participant Count: <span id="spanGroupMembers">0</span></h5>
					</div>
					<div class="row">
						<h5 class="text-info">Total Registered Participant: <span id="spanRegisteredCount">0</span></h5>
					</div>
					
				</div>
			</div>
			

			
			
		</div>
		<div class="ibox-content">
			
			<div class="row">
				

			
				<div class="col-md-12">
					<div class="form-group">
						<label>Select Item</label>
						<select data-placeholder="Choose a item..." class="chosen-select" id="selectItem"  tabindex="2" name="selItem" onchange="get_group_item_member(this.value)">
							<option value="">Select Item</option>
							<?php foreach ($itemDeatils as $key) { ?>
							<option value=<?php echo $key->ItemId ?>><?php echo $key->ItemName ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					
					<div class="form-group">
						<label>Student Register No <span class="text-danger">*</span></label>
						<input type="text" onblur="get_student_details(this.value)" id="txtRegisterNo" name="txtRegisterNo" placeholder="Enter Register No" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Name of Student <span class="text-danger">*</span></label>
						<input type="text" name="txtStudentName" id="txtStudentName" placeholder="Enter Name" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="txtStudentEmail" id="txtStudentEmail" placeholder="Enter email" class="form-control" >
					</div>
					<input type="hidden" name="hdItemType" value="2">
					
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Mobile No <span class="text-danger">*</span></label>
						<input type="text" name="txtMobileNo" id="txtMobileNo" placeholder="Enter mobile no" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Course</label>
						<input type="text" name="txtCourse" id="txtCourse" placeholder="Enter Course" class="form-control">
					</div>
					<div class="form-group">
						<label>Class</label>
						<input type="text" name="txtClass" id="txtClass" placeholder="Enter Class" class="form-control">
					</div>
					
					<div>
						<button class="btn btn-sm btn-primary float-right m-t-n-xs " type="button" id="btnRegisterParticipant">Register Participant <span id="spanParticipantCount">1</span></button>
						<button class="btn btn-sm btn-danger float-right m-t-n-xs m-r-md" type="submit">Reset</button>
					</div>
				</div>
				
			</div>
		
	</div>
</div>
</div>


<input type="hidden" name="hdGroupMembers" id="hdGroupMembers" value=""/>
<input type="hidden" name="hdGroupMembersCurrentCount" id="hdGroupMembersCurrentCount" value="1"/>
<input type="hidden" name="hdCollegeId" id="hdCollegeId" value="<?php echo $collegeId; ?>">
<input type="hidden" name="hdIsStudentExit" id="hdIsStudentExit" value="0">