<div class="col-md-12">
	<div class="ibox">
		<div class="ibox-title">
			<h5>Register Student</h5>
		</div>
		<div class="ibox-content">
			<?php echo form_open('College/insert_students'); ?>
			<div class="row">	
				<div class="col-md-12">
				<?php if($isInsert==1){ ?>
					<div class="alert alert-success alert-dismissable">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<a class="alert-link" href="#">Sucess:</a> Student insert sucessfully  .
					</div>
				<?php }elseif ($isInsert==2) { ?>
					<div class="alert alert-danger">
                                <a class="alert-link" href="#">Faill:</a> <?php echo $this->input->get("msg"); ?>
                            </div>
				<?php } ?>
				</div>
			
				<div class="col-md-12">
					<div class="form-group">
						<label>Select Item</label>
						<select data-placeholder="Choose a item..." class="chosen-select" id="selectItem"  tabindex="2" name="selItem">
							<option value="">Select Item</option>
							<?php foreach ($itemDeatils as $key) { ?>
							<option value=<?php echo $key->ItemId ?>><?php echo $key->ItemName ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					
					<div class="form-group">
						<label>Student Register No</label>
						<input type="text" onblur="get_student_details(this.value)" name="txtRegisterNo" placeholder="Enter Register No" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Name of Student</label>
						<input type="text" name="txtStudentName" id="txtStudentName" placeholder="Enter Name" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="txtStudentEmail" id="txtStudentEmail" placeholder="Enter email" class="form-control" >
					</div>
					<input type="hidden" name="hdItemType" value="2">
					
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Mobile No</label>
						<input type="text" name="txtMobileNo" id="txtMobileNo" placeholder="Enter mobile no" class="form-control" required="">
					</div>
					<div class="form-group">
						<label>Course</label>
						<input type="text" name="txtCourse" id="txtCourse" placeholder="Enter Course" class="form-control">
					</div>
					<div class="form-group">
						<label>Class</label>
						<input type="text" name="txtClass" id="txtClass" placeholder="Enter Class" class="form-control">
					</div>
					
					<div>
						<button class="btn btn-sm btn-primary float-right m-t-n-xs " type="submit"><strong>Register</strong></button>
						<button class="btn btn-sm btn-danger float-right m-t-n-xs m-r-md" type="submit"><strong>Reset</strong></button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>
</div>

<input type="hidden" name="hdCollegeId" id="hdCollegeId" value="<?php echo $collegeId; ?>">
<input type="hidden" name="hdIsStudentExit" id="hdIsStudentExit" value="0">