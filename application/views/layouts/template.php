<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $title; ?></title>

    <link href="<?php echo site_url() ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo site_url() ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>css/style.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

     <link href="<?php echo site_url() ?>img/fav.ico" rel="shortcut icon" type="image/x-icon" />
</head>

<body>

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">

            <?php 
                if($userType=="admin"){
                    $this->load->view('menu/admin_menu');
                }else{
                    $this->load->view('menu/college_menu');
                }
            ?>
        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
              
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome MG University Festival.</span>
                </li>
               

                <li>
                    <a href="<?php echo site_url() ?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo $title; ?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url() ?>Admin/">Home</a>
                        </li>
                        
                        <li class="breadcrumb-item active">
                            <strong><?php echo $title; ?></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="row wrapper wrapper-content animated fadeInRight">
         <?php echo $contents ?>
        </div>
       
        </div>
        </div>


    <!-- Mainly scripts -->
    <script src="<?php echo site_url() ?>js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo site_url() ?>js/popper.min.js"></script>
    <script src="<?php echo site_url() ?>js/bootstrap.js"></script>
    <script src="<?php echo site_url() ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo site_url() ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo site_url() ?>js/inspinia.js"></script>
    <script src="<?php echo site_url() ?>js/plugins/pace/pace.min.js"></script>
   
     <!-- Chosen -->
    <script src="<?php echo site_url() ?>js/plugins/chosen/chosen.jquery.js"></script>
    <!-- Toastr script -->
    <script src="<?php echo site_url() ?>js/plugins/toastr/toastr.min.js"></script>
     <script src="<?php echo site_url() ?>js/mgu.js"></script>
    <script type="text/javascript">
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "progressBar": false,
          "preventDuplicates": true,
          "positionClass": "toast-top-right",
          "onclick": null,
          "showDuration": "200",
          "hideDuration": "500",
          "timeOut": "7000",
          "extendedTimeOut": "200",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
         $('#selectItem').chosen({width: "100%"});
          $('#selectCollege').chosen({width: "100%"});
           $('#SelectType').chosen({width: "100%"});
           $('#selStage').chosen({width: "100%"});
           $('#selDay').chosen({width: "100%"});
    </script>


</body>

</html>
