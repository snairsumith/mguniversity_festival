            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            
                            <span class="block m-t-xs font-bold">College</span>
                            
                            <span class="text-muted text-xs block"><?php echo $this->session->userdata('CollegeName'); ?> </span>
                        </a>
                       
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class=<?php echo $menu=='dashboard'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>College/">
                        <i class="fa fa-th-large"></i> 
                        <span class="nav-label">DashBoard</span>
                         
                    </a>
                </li>

               <li class=<?php echo $menu=='students'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>College/students"><i class="fa fa-university"></i> <span class="nav-label">Students Registration</span>
                        <span class="fa arrow"></span>
                    </a>
                    
                    <ul class="nav nav-second-level collapse">
                         <li class=<?php echo $this->uri->segment(3)=='group'?'active':'' ?>><a href="<?php echo site_url() ?>College/students/group">Group Item</a></li>
                         <li class=<?php echo $this->uri->segment(3)=='single'?'active':'' ?>><a href="<?php echo site_url() ?>College/students/single">Single Item</a></li>
                    </ul>
                </li>
                
                <li class=<?php echo $menu=='report'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>College/generatePDFFile"><i class="fa fa-file"></i> <span class="nav-label">Report</span></a>
                </li>  
                 <!--  <li class=<?php echo $menu=='settings'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>College/settings"><i class="fa fa-file"></i> <span class="nav-label">Change Password</span></a>
                </li>   -->    
                <li>
                    <a href="<?php echo site_url() ?>Common/logout"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
                </li>    
               
               
            </ul>