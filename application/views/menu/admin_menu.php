            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" class="rounded-circle" src="<?php echo site_url() ?>img/profile_small.jpg"/>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            
                            <span class="block m-t-xs font-bold">Admin</span>
                            
                            <span class="text-muted text-xs block">University Admin</span>
                        </a>
                       
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <li class=<?php echo $menu=='home'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>Admin/"><i class="fa fa-th-large"></i> <span class="nav-label">DashBoard</span></a>
                </li>

               <li class=<?php echo $menu=='college'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>Admin/college"><i class="fa fa-university"></i> <span class="nav-label">College</span></a>
                </li>
                <li class=<?php echo $menu=='items'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>Admin/items"><i class="fa fa-medium"></i> <span class="nav-label">Items</span></a>
                </li>    
                <li class=<?php echo $menu=='items'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>Admin/schedule_item"><i class="fa fa-medium"></i> <span class="nav-label">Schedule</span></a>
                </li>    
            
               <li class=<?php echo $menu=='report'?'active':'' ?>>
                    <a href="#"><i class="fa fa-university"></i> <span class="nav-label">Report</span>
                        <span class="fa arrow"></span>
                    </a>
                    
                    <ul class="nav nav-second-level collapse">
                         <li class=<?php echo $menu=='report_clg'?'active':'' ?>><a href="<?php echo site_url() ?>Admin/report_college">By College</a></li>
                          <li class=<?php echo $menu=='report_item'?'active':'' ?>><a href="<?php echo site_url() ?>Admin/report_item">By Item Wise</a></li>
                        <!--  <li ><a href="<?php echo site_url() ?>Admin/report_item">By Item</a></li> -->
                    </ul>
                </li>
                 <li class=<?php echo $menu=='result'?'active':'' ?>>
                    <a href="#"><i class="fa fa-university"></i> <span class="nav-label">Result</span>
                        <span class="fa arrow"></span>
                    </a>
                    
                    <ul class="nav nav-second-level collapse">
                         <li class=<?php echo $menu=='result_view'?'active':'' ?>><a href="<?php echo site_url() ?>Admin/view_result">View Result</a></li>
                          <li class=<?php echo $menu=='result_add'?'active':'' ?>><a href="<?php echo site_url() ?>Admin/result_add">Add New</a></li>
                        
                    </ul>
                </li>
       
                 
                <li class=<?php echo $menu=='logout'?'active':'' ?>>
                    <a href="<?php echo site_url() ?>Common/logout"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
                </li>    
               
                   
            </ul>