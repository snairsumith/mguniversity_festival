-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2019 at 03:20 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mguniversity_festival_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE `college` (
  `CollegeId` int(10) NOT NULL,
  `CollegeName` varchar(500) NOT NULL,
  `Address` text,
  `EmailId` varchar(250) NOT NULL,
  `PhoneNo` varchar(20) DEFAULT NULL,
  `MobileNo` varchar(20) DEFAULT NULL,
  `Password` varchar(300) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsActive` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`CollegeId`, `CollegeName`, `Address`, `EmailId`, `PhoneNo`, `MobileNo`, `Password`, `CreatedDate`, `IsActive`) VALUES
(37, 'New Clg', 'Karukaparambil Building', 'neclg@gmail.com', '9526909898', '9526909898', '', '2019-01-31 05:53:01', 1),
(38, 'ss coll', 'thethothil house', 'ssclg@minusbugs.com', '9656761101', '9656761101', 'ss_mguk@2019', '2019-02-06 00:18:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `ItemId` int(10) NOT NULL,
  `ItemName` varchar(300) NOT NULL,
  `ItemDescription` text NOT NULL,
  `ItemTypeId` int(2) NOT NULL COMMENT 'Group-1,Single2',
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`ItemId`, `ItemName`, `ItemDescription`, `ItemTypeId`, `CreatedDate`) VALUES
(1, 'Light Vocal Indian\r\nMale ', 'Light Vocal Indian\r\nMale ', 1, '2019-01-28 14:07:41'),
(2, 'Light Vocal Indian\r\nFemale ', 'Light Vocal Indian\r\nFemale ', 1, '2019-01-28 14:08:32'),
(3, 'Classical Vocal Male', 'Classical Vocal Male', 1, '2019-01-28 14:08:32'),
(4, 'Test', 'Test', 1, '2019-02-05 15:20:03'),
(5, 'Test', 'Test', 2, '2019-02-05 15:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `LoginId` int(10) NOT NULL,
  `EmailId` varchar(200) NOT NULL,
  `Password` varchar(300) NOT NULL,
  `RoleType` int(2) NOT NULL,
  `CollegeId` int(10) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`LoginId`, `EmailId`, `Password`, `RoleType`, `CollegeId`, `CreatedDate`) VALUES
(26, 'admin@gmail.com', 'admin', 1, 0, '2019-01-29 14:29:27'),
(31, 'neclg@gmail.com', '123', 2, 37, '2019-01-31 05:53:01'),
(32, 'ssclg@minusbugs.com', 'ss_mguk@2019', 2, 38, '2019-02-06 00:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `StudentId` int(10) NOT NULL,
  `AdmissionNo` varchar(200) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Class` varchar(100) NOT NULL,
  `Course` varchar(100) NOT NULL,
  `CollegeId` int(10) NOT NULL,
  `PhoneNo` varchar(100) NOT NULL,
  `EmailId` varchar(200) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`StudentId`, `AdmissionNo`, `Name`, `Class`, `Course`, `CollegeId`, `PhoneNo`, `EmailId`, `CreatedOn`) VALUES
(1, 'Ad123', 'sumith s nair', 'CT', 'CT', 37, '9526909898', 'sumith@gmail.com', '2019-01-31 14:17:14'),
(8, 'Ad3456', 'Hari PS', 'Cg', 'CT', 38, '9656761101', 'hari@gmail.com', '2019-01-31 18:12:59'),
(9, 'Adm2', 'sum', 'f', 'f', 37, '9656761101', 'sumith@minusbugs.com', '2019-02-06 01:15:44'),
(10, 'Ad1123', 'sumith s nair', 'Cg', 'CT', 37, '9656761101', 'sumith@minusbugs.com', '2019-02-06 01:16:08'),
(11, 'Ad3456', 'sumith s nair', 'Cg', 'CT', 37, '9656761101', 'sumith@minusbugs.com', '2019-02-06 01:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `students_item`
--

CREATE TABLE `students_item` (
  `StudentItemId` int(10) NOT NULL,
  `StudentId` int(10) NOT NULL,
  `ItemId` int(10) NOT NULL,
  `ItemType` int(10) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students_item`
--

INSERT INTO `students_item` (`StudentItemId`, `StudentId`, `ItemId`, `ItemType`, `CreatedDate`) VALUES
(1, 1, 1, 2, '2019-01-31 14:17:14'),
(8, 8, 3, 2, '2019-01-31 18:12:59'),
(9, 9, 1, 2, '2019-02-06 01:15:44'),
(11, 11, 2, 2, '2019-02-06 01:19:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `college`
--
ALTER TABLE `college`
  ADD PRIMARY KEY (`CollegeId`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`ItemId`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`LoginId`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`StudentId`);

--
-- Indexes for table `students_item`
--
ALTER TABLE `students_item`
  ADD PRIMARY KEY (`StudentItemId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `college`
--
ALTER TABLE `college`
  MODIFY `CollegeId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `ItemId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `LoginId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `StudentId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `students_item`
--
ALTER TABLE `students_item`
  MODIFY `StudentItemId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
