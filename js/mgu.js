// var baseurl="http://mgukal;82olsavam19.com/";
var baseurl="http://localhost/mguniversity_festival/";

$("#btnCollegeSave").click(function(event) {
	

	  var isValid=true;
	  var college_name = $("#txtCollegeName").val();
	  var college_email= $("#txtEmail").val();
	  var college_phno = $("#txtPhoneNo").val();
	  var college_mobno= $("#txtMobileNo").val();
	  var college_pwd  = $("#txtPassword").val();
	  var college_cpwd = $("#txtCPassword").val();
	  var college_addre= $("#txtAddress").val();
	  
	  if(college_name==""){
	  	isValid=false;
	  	$("#cllegeName-error").text("College Name Required");
	  	$("#txtCollegeName").addClass('error');
	  }else{
	  	
	  	$("#cllegeName-error").text("");
	  	$("#txtCollegeName").removeClass('error');
	  }

	  

	  
	  if(college_email==""){
	  	isValid=false;
	  	$("#cllegeEmail-error").text("College Email Required");
	  	$("#txtEmail").addClass('error');
	  }else if(IsEmail(college_email)==false){
	    isValid=false;
	  	$("#cllegeEmail-error").text("Invalid email id ");
	  	$("#txtEmail").addClass('error');
	  }else if(isEmailExist(college_email)){
	    isValid=false;
	  	$("#cllegeEmail-error").text("Email is already exist ");
	  	$("#txtEmail").addClass('error');
	  }
	  else{
	  	
	  	$("#cllegeEmail-error").text("");
	  	$("#txtEmail").removeClass('error');
	  }

	  if(college_pwd==""){
	  	isValid=false;
	  	$("#cllegePwd-error").text("Password Required");
	  	$("#txtPassword").addClass('error');
	  }else  if(college_pwd!=college_cpwd){
	  	isValid=false;
	  	$("#cllegeCPwd-error").text("Password Missmatch");
	  	$("#txtPassword").addClass('error');
	  	$("#txtCPassword").addClass('error');
	  }else{
	  	
	  	$("#cllegeCPwd-error").text("");
	  	$("#cllegePwd-error").text("");
	  	$("#txtPassword").removeClass('error');
	  	$("#txtCPassword").removeClass('error');
	  }
	 



	  if(isValid){
	  	var d = new Date();
	  	var url=baseurl+'Festival_api/college';
	  	var data={'name':college_name,'email':college_email,'address':college_addre,'mobile':college_mobno,'phno':college_phno,'password':college_pwd}
	  	$.post(url,data,function(data){

	  		if(data.resultCode==1){
		  		toastr.success("College details inserted sucessfully","Sucess")
		  		var html="<tr><td>"+college_name+"</td><td>"+college_addre+"</td><td>"+college_email+"</td><td>"+college_phno+"</td><td>"+college_mobno+"</td><td>"+college_pwd+"</td><td><button class='btn-info btn btn-xs' data-toggle='modal' data-target='#collegeEditModel' onclick='fetchCollegeDeatild("+data.CollegeId+")'>Edit</button></tr>";
		  		$("#tbody_college").append(html)
		  		$('#collegeAddModel').modal('toggle');
		  		$("#txtCollegeName").val("");
				$("#txtEmail").val("");
				$("#txtPhoneNo").val("");
				$("#txtMobileNo").val("");
				$("#txtPassword").val("");
				$("#txtCPassword").val("");
				$("#txtAddress").val("");
	  		}else{
	  			toastr.error(data.message,"Error")
	  		}
	  		
	  
	  	});
	  }


});

$("#btnLogin").click(function(event) {
	

	var email = $("#txtEmail").val();
	var pwd   = $("#txtPassword").val();
	var url=baseurl+'Festival_api/login?email='+email+'&password='+pwd+'';
	  	$.get(url,function(data){
	  		if(data.resultCode==1){
	  			if(data.data[0].RoleType==1){
	  				window.location.href =baseurl+"Admin";
	  			}else if(data.data[0].RoleType==2){

	  			}
	  		}else{
	  			
	  		}
	  	});
});


 function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}
  function isEmailExist(email){
  	var url=baseurl+'Festival_api/college?email='+email+'';
	  	$.get(url,function(data){
	  		if(data.resultCode==1){
	  			return true;
	  		}else{
	  			return false;
	  		}
	  	});
    }

$("#txtCollegeName").change(function(){

	var collegename= $("#txtCollegeName").val();
	var rand = Math.floor(Math.random() * 110);
	 var firstchar = collegename.substring(0,2).toLowerCase()+rand+"_mguk@2019";
	 $("#txtPassword").val(firstchar);
	 $("#txtCPassword").val(firstchar);
});
  function fetchStudentsItems(studentId){
  	var CollegeId = $("#hdCollegeId").val();
	var url=baseurl+'Festival_api/item_deatils_by_student?studentid='+studentId+'&itemtype=2&collegeid='+CollegeId;
	var html="";
	$.get(url,function(data){
		if(data.resultCode==1){
			$.each(data.data, function( index, value ) {
				html=html+"<tr id=tr_"+value.StudentItemId+"><td>"+ (index+1)+"</td><td>"+value.ItemName+"</td><td>"+value.CreatedDate+"</td><td> <a onClick='delete_registerd_Item("+value.StudentItemId+")' href='#' class='btn btn-xs btn-primary'><i class='fa fa-trash'></i> Delete </a> </td></tr>";
			  });
			  $("#tbody_student_item").html(html);
		}
		
	});
  }

function fetchCollegeDeatild(id){
	var url=baseurl+'Festival_api/college_by_id/'+id;
	$.get(url,function(data){
		if(data.resultCode==1){
			$("#hdCollegeId").val(id);
			$("#txtEditCollegeName").val(data.data[0].CollegeName);
			$("#txtEditEmail").val(data.data[0].EmailId);
			$("#txtEditPhoneNo").val(data.data[0].PhoneNo);
			$("#txtEditMobileNo").val(data.data[0].MobileNo);
			$("#txtEditPassword").val(data.data[0].Password);
			$("#txtEditCPassword").val(data.data[0].Password);
			$("#txtEditAddress").val(data.data[0].Address);

		}
		

	});
}


function get_student_details(admission_id){
	var collegeid= $("#hdCollegeId").val();
		var url=baseurl+'Festival_api/get_student?admissionno='+admission_id+'&collegeid='+collegeid;
		$.get(url,function(data){
		if(data.resultCode==1){
			
			$("#txtStudentName").val(data.data[0].Name);
			$("#txtStudentEmail").val(data.data[0].EmailId);
			$("#txtMobileNo").val(data.data[0].PhoneNo);
			$("#txtCourse").val(data.data[0].Course);
			$("#txtClass").val(data.data[0].Class);
			$("#hdIsStudentExit").val(1);
		}else{
			$("#txtStudentName").val('');
			$("#txtStudentEmail").val('');
			$("#txtMobileNo").val('');
			$("#txtCourse").val('');
			$("#txtClass").val('');
			$("#hdIsStudentExit").val(0);
		}
		

	});

}

$("#btnCollegeEdit").click(function(event) {
	

	  var isValid=true;
	  var college_name = $("#txtEditCollegeName").val();
	  var college_email= $("#txtEditEmail").val();
	  var college_phno = $("#txtEditPhoneNo").val();
	  var college_mobno= $("#txtEditMobileNo").val();
	  var college_pwd  = $("#txtEditPassword").val();
	  var college_cpwd = $("#txtEditCPassword").val();
	  var college_addre= $("#txtEditAddress").val();
	  var college_id   =  $("#hdCollegeId").val();
	  
	  if(college_name==""){
	  	isValid=false;
	  	$("#EditcllegeName-error").text("College Name Required");
	  	$("#txtEditCollegeName").addClass('error');
	  }else{
	  	
	  	$("#EditcllegeName-error").text("");
	  	$("#txtEditCollegeName").removeClass('error');
	  }
	  if(college_email==""){
	  	isValid=false;
	  	$("#EditcllegeEmail-error").text("College Email Required");
	  	$("#txtEditEmail").addClass('error');
	  }else if(IsEmail(college_email)==false){
	    isValid=false;
	  	$("#EditcllegeEmail-error").text("Invalid email id ");
	  	$("#txtEditEmail").addClass('error');
	  }else if(isEmailExist(college_email)){
	    isValid=false;
	  	$("#EditcllegeEmail-error").text("Email is already exist ");
	  	$("#txtEditEmail").addClass('error');
	  }
	  else{
	  	
	  	$("#EditcllegeEmail-error").text("");
	  	$("#txtEditEmail").removeClass('error');
	  }

	  if(college_pwd==""){
	  	isValid=false;
	  	$("#EditcllegePwd-error").text("Password Required");
	  	$("#txtEditPassword").addClass('error');
	  }else  if(college_pwd!=college_cpwd){
	  	isValid=false;
	  	$("#EditcllegeCPwd-error").text("Password Missmatch");
	  	$("#txtEditPassword").addClass('error');
	  	$("#txtEditCPassword").addClass('error');
	  }else{
	  	
	  	$("#EditcllegeCPwd-error").text("");
	  	$("#EditcllegePwd-error").text("");
	  	$("#txtEditPassword").removeClass('error');
	  	$("#txtEditCPassword").removeClass('error');
	  }
	 



	  if(isValid){
	  	var d = new Date();
	  	var url=baseurl+'Festival_api/college_edit';
	  	var data={'collegeid':college_id,'name':college_name,'email':college_email,'address':college_addre,'mobile':college_mobno,'phno':college_phno,'password':college_pwd}
	  	$.post(url,data,function(data){

	  		if(data.resultCode==1){
		  		toastr.success("College details updated sucessfully","Sucess")
		  		location.reload(true);
	  		}else{
	  			toastr.error(data.message,"Error")
	  		}
	  		
	  
	  	});
	  }


});

function get_group_item_member(itemid){
	var url=baseurl+'Festival_api/item/'+itemid
		$.get(url,function(data){
		if(data.resultCode==1){
			$("#hdGroupMembers").val(data.data[0].Members);
			$("#spanGroupMembers").text(data.data[0].Members);
			$("#hdGroupMembersCurrentCount").val(0);
			$("#btnRegisterParticipant").text("Register Participant ");
		}
	});
		get_registerd_participant_group(itemid);
		$("#btnRegisterParticipant").removeAttr("disabled");
}

function get_registerd_participant_group(itemid){
	var collegeid=$("#hdCollegeId").val();
	var url=baseurl+'Festival_api/item_registered_participant_group?collegeid='+collegeid+'&itemid='+itemid;
		$.get(url,function(data){
			if(data.resultCode==1){
				totalRegisterdCount=parseInt(data.count);
				if(data.count>0){
					
					$("#spanRegisteredCount").text(data.count);
					
				}else{
					$("#spanRegisteredCount").text(0);
				}
				
			}else{
				$("#spanRegisteredCount").text(0);
			}
		})
}


$("#btnRegisterParticipant").click(function(event) {
	var admissionno=$("#txtRegisterNo").val();
	var name=$("#txtStudentName").val();
	var email=$("#txtStudentEmail").val();
	var mobile=$("#txtMobileNo").val();
	var course=$("#txtCourse").val();
	var stud_class=$("#txtClass").val();
	var member =$("#hdGroupMembers").val();
	var collegeid =$("#hdCollegeId").val();
	var isstudentexit =$("#hdIsStudentExit").val();
	var currentcout =parseInt($("#hdGroupMembersCurrentCount").val());
	var itemid =$("#selectItem").val();
	var data={'collegeid':collegeid,'itemid':itemid,'admno':admissionno,'name':name,'class':stud_class,'course':course,'mobile':mobile,'email':email,'isstudentexit':isstudentexit};
	var url=baseurl+'Festival_api/insert_group_participant';
	$.post(url,data,function(data){
		if(data.resultCode==1){
			
			get_registerd_participant_group(itemid);
			$("#txtRegisterNo").val('');
			$("#txtStudentName").val('');
			$("#txtStudentEmail").val('');
			$("#txtMobileNo").val('');
			$("#txtCourse").val('');
			$("#txtClass").val('');
			currentcout=currentcout+1;
			if(member>currentcout){
				$("#hdGroupMembersCurrentCount").val(currentcout);
				$("#spanParticipantCount").text(currentcout);
			}else{
				$("#btnRegisterParticipant").attr("disabled", "disabled");
				toastr.info("All Participant registration completed");
				
			}
			toastr.success("Participant "+currentcout+" inserted sucessfully ","Sucess");
			
		}else{
			toastr.error(data.message,"Error");
			$("#txtRegisterNo").val('');
			$("#txtStudentName").val('');
			$("#txtStudentEmail").val('');
			$("#txtMobileNo").val('');
			$("#txtCourse").val('');
			$("#txtClass").val('');
		}
	});
});

function fetchStudentDetailsByGroup(ItemId){
	var collegeid =$("#hdCollegeId").val();
	var html="";
	var url=baseurl+'Festival_api/get_all_students_by_group_item?collegeid='+collegeid+'&itemid='+ItemId;
		$.get(url,function(data){
			if(data.resultCode==1){
			$.each(data.data, function( index, value ) {
				if(value.CollegeId==collegeid){
				html=html+"<tr id=tr_"+value.StudentItemId+"><td>"+ (index+1)+"</td><td>"+value.AdmissionNo+"</td><td>"+value.Name+"</td><td>"+value.PhoneNo+"</td><td>"+value.Class+"</td><td> <a onClick='delete_registerd_Item("+value.StudentItemId+")' href='#' class='btn btn-xs btn-primary'><i class='fa fa-trash'></i> Delete </a> </td></tr>";
			}
			  });
			  $("#tbody_group_student_item").html(html);
			}
		});
}

function delete_registerd_Item(id){
	// var CollegeId = $("#hdCollegeId").val()
	var url=baseurl+'Festival_api/delete_registerd_item/'+id;
		$.get(url,function(data){
			if(data.resultCode==1){
				toastr.success(data.message,"Sucess")
				$("#tr_"+id).css('display', 'none');
			}else{
				toastr.error(data.message,"Error")
			}
		})
}

function get_all_registerd_participant_by_item(itemid){
	
	html="";
	$("#StudentDeatilsBind").html(html);
	var url=baseurl+'Festival_api/fetch_college_details_by_item/'+itemid;
	$.get(url,function(data){
		if(data.resultCode==1){
			$.each(data.data, function( index, value ) {

				html=html+"<ul><li> <h4>"+(index+1)+"</h4>";
				html=html+"<h3>Name Of Participant : <span>"+value.Name+"</span></h3>";
				html=html+"<h3><b>College Name : <span>"+value.CollegeName+"</span></b></h3>";
				html=html+"<h3>Register No : <span>"+value.AdmissionNo+"</span></h3>";
				html=html+"<h3>Class  : <span>"+value.Class+"</span></h3>";
				html=html+"<h3>Course : <span>"+value.Course+"</span></h3>";
				html=html+"<h3>Email Id : <span>"+value.EmailId+"</span></h3>";
				html=html+"<h3>Phone No : <span>"+value.PhoneNo+"</span></h3>";
				html=html+"</li></ul>";
			});
			$("#StudentDeatilsBind").html(html);
		}
	})
			
function get_itemwiseResult(id){
	alert(id);
}
		

	
}